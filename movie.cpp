//===========================================================================
//
//
//
//
//===========================================================================

#ifdef _WIN32
#include <io.h>
#else
#include <unistd.h>
static inline off_t tell(int fd)
{
	return lseek(fd, 0, SEEK_CUR);
}
#endif
#include <stdio.h>
#include <string.h>
//#include <dos.h>
//#include <FCNTL.H>

#include "3d_def.h"
#include "jm_io.h"
#include "an_codes.h"

//===========================================================================
//
//
//
//
//===========================================================================


//#define  DRAW_TO_FRONT

//
// Various types for various purposes...
//

typedef enum
{
  	FADE_NONE,
   FADE_SET,
   FADE_IN,
   FADE_OUT,
   FADE_IN_FRAME,
   FADE_OUT_FRAME,
} FADES;


typedef enum
{
    MV_NONE,
    MV_FILL,
    MV_SKIP,
    MV_READ,

} MOVIE_FLAGS;

//===========================================================================
//
//											VARIABLES
//
//===========================================================================


// Movie File variables

int Movie_FHandle;

// Fade Variables

FADES fade_flags, fi_type, fo_type;
byte	fi_rate,fo_rate;

// MOVIE_GetFrame & MOVIE_LoadBuffer variables

memptr MovieBuffer;					// Ptr to Allocated Memory for Buffer
uint32_t BufferLen;			// Len of MovieBuffer (Ammount of RAM allocated)
uint32_t PageLen;				// Len of data loaded into MovieBuffer
byte * BufferPtr;				// Ptr to next frame in MovieBuffer
byte * NextPtr;   				// Ptr Ofs to next frame after BufferOfs

boolean MorePagesAvail;				// More Pages avail on disk?

//

MOVIE_FLAGS  movie_flag;
boolean ExitMovie;
boolean EverFaded;
long seek_pos;
char movie_reps;
ControlInfo ci;
SDL_Color movie_palette[256];


//
// MOVIE Definitions for external movies
//
// NOTE: This list is ordered according to mv_???? enum list.
//


MovieStuff_t Movies[] =
{
	{{"IANIM."},1,3,0,0,200},				//mv_intro
	{{"EANIM."},1,30,0,0,200},				//mv_final
};


//===========================================================================
//
//										LOCAL PROTO TYPES
//
//===========================================================================

void JM_MemToScreen(void);
void JM_ClearVGAScreen(byte fill);
void FlipPages(void);
boolean CheckFading(void);
boolean CheckPostFade(void);


//===========================================================================
//
//										   FUNCTIONS
//
//===========================================================================


//---------------------------------------------------------------------------
// SetupMovie() - Inits all the internal routines for the Movie Presenter
//
//
//
//---------------------------------------------------------------------------
void SetupMovie(MovieStuff_t *MovieStuff)
{
    movie_reps = MovieStuff->rep;
    movie_flag = MV_FILL;
    LastScan = 0;										  
    PageLen = 0;
    MorePagesAvail = true;
    ExitMovie = false;
    EverFaded = false;
    IN_ClearKeysDown();

    memcpy(movie_palette, MovieStuff->palette, sizeof(SDL_Color) * 256);
    // TODO: This
    //JM_VGALinearFill(screenloc[0],3*80*208,0);

    VL_FillPalette (0,0,0);
    LastScan = 0;

    // Find out how much memory we have to work with..

    BufferLen = 500000; // random-ass 8MB?? (idk)
    BufferLen -= 65535;						// HACK: Save some room for sounds - This is cludgey

    if (BufferLen < 64256)
        BufferLen = 64256;

    MovieBuffer = malloc(BufferLen);
	CHECKMALLOCRESULT(&MovieBuffer);
}


//---------------------------------------------------------------------------
// void ShutdownMovie(void)
//---------------------------------------------------------------------------
void ShutdownMovie(void)
{
    free(MovieBuffer);
    close (Movie_FHandle);
}

//---------------------------------------------------------------------------
// void JM_DrawBlock()
//
// dest_offset = Correct offset value for memory location for Paged/Latch mem
//
// byte_offset = Offset for the image to be drawn - This address is NOT
//					  a calculated Paged/Latch value but a byte offset in
//					  conventional memory.
//
// source		= Source image of graphic to be blasted to latch memory.  This
//					  pic is NOT 'munged'
//
// length		= length of the source image in bytes
//---------------------------------------------------------------------------
void JM_DrawBlock(unsigned dest_offset,unsigned byte_offset,char *source,unsigned length)
{
    byte numplanes;
    byte mask,plane;
    char *dest_ptr;
    char *source_ptr;
    char *dest;
    char *end_ptr;
    unsigned count,total_len;


    end_ptr = source+length;

    //
    // Byte offset to determine our starting page to write to...
    //

    mask = 1<<(byte_offset & 3);

    //
    // Check to see if we are writting more than 4 bytes (to loop pages...)
    //

    if (length >= 4)
        numplanes = 4;
    else
        numplanes = length; 

   //
   // Compute our DEST memory location
   //

    byte *ptr = VL_LockSurface(curSurface);
	if(ptr == NULL) return;
    dest = (char *)(ptr + scaleFactor * (dest_offset * curPitch + byte_offset));
    VL_UnlockSurface(curSurface);

   //
   // Move that memory.
   //

	for (plane = 0; plane<numplanes; plane++)
	{
   	    dest_ptr = dest;
	    source_ptr = source+plane;

	    mask <<= 1;
	    if (mask == 16) {
		    mask = 1;
            dest++;
        }

	    for (count=0;count<length,source_ptr < end_ptr;count+=4,dest_ptr++,source_ptr+=4)
      	    *dest_ptr = *source_ptr;
	}
}



//---------------------------------------------------------------------------
// MOVIE_ShowFrame() - Shows an animation frame
//
// PARAMETERS: pointer to animpic
//---------------------------------------------------------------------------
void MOVIE_ShowFrame (byte *inpic)
{
    anim_chunk *ah;

    if (inpic == NULL)
        return;
    
    byte *vbuf = VL_LockSurface(screenBuffer); 
    do {
        ah = (anim_chunk *)inpic;

        if (ah->opt == 0)
		    break;

        inpic += sizeof(anim_chunk);
        
        int x = ah->offset % 320; 
        int y = ah->offset / 320; 
        byte *vbufptr = vbuf + y * scaleFactor * bufferPitch + x * scaleFactor; 
        byte *bufptr = inpic;
        for(int i = 0; i < ah->length; vbufptr += scaleFactor * (bufferPitch - 320), x = 0) 
        { 
            for(int j = x; i < ah->length && j < 320; i++, j++, vbufptr += scaleFactor) 
            { 
                byte col = *bufptr++; 
                for(unsigned m = 0; m < scaleFactor; m++) 
                    for(unsigned n = 0; n < scaleFactor; n++) 
                        vbufptr[m * bufferPitch + n] = col; 
            } 
        } 
        // TODO: This
	    //JM_DrawBlock(0, ah->offset, (char *)inpic, ah->length);
        inpic += ah->length;
    } while(true);
     VL_UnlockSurface(screenBuffer);
}



//---------------------------------------------------------------------------
// MOVIE_LoadBuffer() - Loads the RAM Buffer full of graphics...
//
// RETURNS:  true  	- MORE Pages avail on disk..
//				 false   - LAST Pages from disk..
//
// PageLen = Length of data loaded into buffer
//
//---------------------------------------------------------------------------

boolean MOVIE_LoadBuffer()
{
    anim_frame blk;
    int32_t chunkstart;
    byte *frame;
    //uint32_t free_space;

    NextPtr = BufferPtr = frame = (byte *)MovieBuffer; // This aint workin // old code: NextPtr = BufferPtr = frame = MK_FP(MovieBuffer,0);
    //free_space = BufferLen;

	while (true) {
   	    chunkstart = tell(Movie_FHandle);

        if (!read(Movie_FHandle, &blk, sizeof(anim_frame)))
            AN_ERROR(AN_BAD_ANIM_FILE);

        if (blk.code == AN_END_OF_ANIM)
            return(false);

        if (true/*free_space>=(blk.recsize+sizeof(anim_frame))*/) {
            memcpy(frame, &blk, sizeof(anim_frame));

            //free_space -= sizeof(anim_frame);
            frame += sizeof(anim_frame);
            PageLen += sizeof(anim_frame);

            if (blk.recsize && !read(Movie_FHandle, (byte *)frame, blk.recsize))
	            AN_ERROR(AN_BAD_ANIM_FILE);

            //free_space -= blk.recsize;
            frame += blk.recsize;
            PageLen += blk.recsize;
        }
        //else {
        //    lseek(Movie_FHandle,chunkstart,SEEK_SET);
        //    free_space = 0;
        //}
    }

    return(true);
}


//---------------------------------------------------------------------------
// MOVIE_GetFrame() - Returns pointer to next Block/Screen of animation
//
// PURPOSE: This function "Buffers" the movie presentation from allocated
//				ram.  It loads and buffers incomming frames of animation..
//
// RETURNS:  0 - Ok
//				 1 - End Of File
//---------------------------------------------------------------------------
short MOVIE_GetFrame() {
    unsigned ReturnVal;
    anim_frame blk;

    if (PageLen == 0) {
        if (MorePagesAvail) {
	        MorePagesAvail = MOVIE_LoadBuffer(); // MOVIE_LoadBuffer(Movie_FHandle);
        }
        else {
            return(1);
        }
    }

    BufferPtr = NextPtr;
    memcpy(&blk, BufferPtr, sizeof(anim_frame));
    PageLen-=sizeof(anim_frame);
    PageLen-=blk.recsize;
    NextPtr = BufferPtr+sizeof(anim_frame)+blk.recsize;
    return(0);
}



//---------------------------------------------------------------------------
// MOVIE_HandlePage() - This handles the current page of data from the
//								ram buffer...
//
// PURPOSE: Process current Page of anim.
//
//
// RETURNS:
//
//---------------------------------------------------------------------------
void MOVIE_HandlePage(MovieStuff_t *MovieStuff)
{
    anim_frame blk;
    byte *frame;
    unsigned wait_time;

	memcpy(&blk,BufferPtr,sizeof(anim_frame));
	BufferPtr+=sizeof(anim_frame);
    frame = BufferPtr;

	IN_ReadControl(0,&ci);

    switch (blk.code) {
        //-------------------------------------------
        //
        //
        //-------------------------------------------

	 	case AN_SOUND:				// Sound Chunk
		{
            unsigned sound_chunk;
            sound_chunk = *(unsigned *)frame;
            SD_PlaySound((soundnames)sound_chunk);
            BufferPtr+=blk.recsize;
        }
        break;  


      //-------------------------------------------
      //
      //
      //-------------------------------------------

#if 0
		case MV_CNVT_CODE('P','M'):				// Play Music
		{
            unsigned song_chunk;
            song_chunk = *(unsigned *)frame;
            SD_MusicOff();

			if (!audiosegs[STARTMUSIC+musicchunk])
			{
//				MM_BombOnError(false);
				CA_CacheAudioChunk(STARTMUSIC + musicchunk);
//				MM_BombOnError(true);
			}

			if (mmerror)
				mmerror = false;
			else
			{
				MM_SetLock(&((memptr)audiosegs[STARTMUSIC + musicchunk]),true);
				SD_StartMusic((MusicGroup far *)audiosegs[STARTMUSIC + musicchunk]);
			}

            BufferPtr+=blk.recsize;
        }
        break;
#endif


      //-------------------------------------------
      //
      //
      //-------------------------------------------

	 	case AN_FADE_IN_FRAME:				// Fade In Page
            VL_FadeIn(0,255,movie_palette,30);
		    fade_flags = FADE_NONE;
            EverFaded = true;
		    screenfaded = false;
        break;



        //-------------------------------------------
        //
        //
        //-------------------------------------------

	    case AN_FADE_OUT_FRAME:				// Fade Out Page
		    VW_FadeOut();
		    screenfaded = true;
		    fade_flags = FADE_NONE;
        break;


        //-------------------------------------------
        //
        //
        //-------------------------------------------

	    case AN_PAUSE:				// Pause
	    {
      	    uint16_t vbls;
            vbls = *(uint16_t *)frame;
		    //IN_UserInput(vbls);
            Delay(vbls); 
            BufferPtr+=blk.recsize;
        }
        break;



      //-------------------------------------------
      //
      //
      //-------------------------------------------

   	    case AN_PAGE:				// Graphics Chunk
#if 1
             if (movie_flag == MV_FILL)
             {
                // First page comming in.. Fill screen with fill color...
                //
                // movie_flag = MV_READ;	// Set READ flag to skip the first frame on an anim repeat
                movie_flag = MV_NONE;	// Set READ flag to skip the first frame on an anim repeat
                // TODO: This
                VWB_Bar(0, 0, 320, 200, (byte)*frame);
		        //JM_VGALinearFill(screenloc[0],3*80*208,*frame);
                frame++;
             }
             else {
#endif 
                // TODO: This
		        //VL_LatchToScreen(displayofs+ylookup[MovieStuff->start_line], 320>>2, MovieStuff->end_line-MovieStuff->start_line, 0, MovieStuff->start_line);
                
             }
            MOVIE_ShowFrame(frame);

#if 0  
            if (movie_flag == MV_READ)
            {
            seek_pos = tell(Movie_FHandle);
            movie_flag = MV_NONE;
            }
#endif 
   		    //FlipPages();
            VW_UpdateScreen();

            // TODO: This is not quite added correctly
           /* if (lasttimecount < MovieStuff->ticdelay)
            {
	            wait_time = MovieStuff->ticdelay - GetTimeCount();
	            Delay(wait_time);
            }
            else*/
	            Delay(5);

		    //lasttimecount = 0;

			if ((!screenfaded) && (ci.button0 || ci.button1 || LastScan))
			{
				ExitMovie = true;
				if (EverFaded)					// This needs to be a passed flag...
				{
					VW_FadeOut();
					screenfaded = true;
				}
			}
		break;


#if 0
      //-------------------------------------------
      //
      //
      //-------------------------------------------

		case AN_PRELOAD_BEGIN:			// These are NOT handled YET!
		case AN_PRELOAD_END:
		break;

#endif
      //-------------------------------------------
      //
      //
      //-------------------------------------------

		case AN_END_OF_ANIM:
			ExitMovie = true;
		break;


      //-------------------------------------------
      //
      //
      //-------------------------------------------

		default:
			AN_ERROR(HANDLEPAGE_BAD_CODE);
		break;
   }
}


//---------------------------------------------------------------------------
// MOVIE_Play() - Playes an Animation
//
// RETURNS: true  - Movie File was found and "played"
//				false - Movie file was NOT found!
//---------------------------------------------------------------------------
bool MOVIE_Play(MovieStuff_t *MovieStuff)
{
	// Init our Movie Stuff...
   //

   SetupMovie(MovieStuff);

   // Start the anim process
   //

   if ((Movie_FHandle = open(MovieStuff->FName, O_RDONLY|O_BINARY)) == -1)	  
     	return(false);

    while (movie_reps && (!ExitMovie)) {
#if 0 	
        if (movie_flag == MV_SKIP)
	        if (lseek(Movie_FHandle, seek_pos, SEEK_SET) == -1)
                return(false);
#endif
	    for (;!ExitMovie;) {
            if (MOVIE_GetFrame()) // MOVIE_GetFrame(Movie_FHandle)
                break;

            MOVIE_HandlePage(MovieStuff);
        }

        movie_reps--;
        movie_flag = MV_SKIP;
    }

   ShutdownMovie();

   return(true);
}

//--------------------------------------------------------------------------
// FlipPages()
//---------------------------------------------------------------------------
void FlipPages(void)
{

#ifndef DRAW_TO_FRONT
    SDL_BlitSurface(screenBuffer, NULL, screen, NULL);
    SDL_Flip(screen);
#endif

}