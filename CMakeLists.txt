cmake_minimum_required(VERSION 2.6)
project(Blake4SDL)

add_definitions(-DUSE_GPL)

find_package(SDL REQUIRED)
find_package(SDL_mixer REQUIRED)
include_directories(${SDLMIXER_INCLUDE_DIR} ${SDL_INCLUDE_DIR})

# Check for some non-standard functions
include(CheckFunctionExists)
check_function_exists(stricmp STRICMP_EXISTS)
check_function_exists(strnicmp STRNICMP_EXISTS)
if(NOT STRICMP_EXISTS)
	add_definitions(-Dstricmp=strcasecmp)
endif(NOT STRICMP_EXISTS)
if(NOT STRNICMP_EXISTS)
	add_definitions(-Dstrnicmp=strncasecmp)
endif(NOT STRNICMP_EXISTS)

add_executable(blake4sdl WIN32
	dosbox/dbopl.cpp
	3d_act1.cpp
	3d_act2.cpp
	3d_agent.cpp
	3d_debug.cpp
	3d_draw.cpp
	3d_draw2.cpp
	3d_game.cpp
	3d_inter.cpp
	3d_main.cpp
	3d_menu.cpp
	3d_msgs.cpp
	3d_play.cpp
	3d_scale.cpp
	3d_state.cpp
	id_ca.cpp
	id_in.cpp
	id_pm.cpp
	id_sd.cpp
	id_us_1.cpp
	id_vh.cpp
	id_vl.cpp
	jm_cio.cpp
	jm_free.cpp
	jm_io.cpp
	jm_lzh.cpp
	jm_tp.cpp
	movie.cpp
	signon.cpp
)

target_link_libraries(blake4sdl ${SDL_LIBRARY} ${SDLMIXER_LIBRARY})
