// 3D_DEBUG.C

#include "3d_def.h"
#pragma hdrstop

/*
=============================================================================

						 LOCAL CONSTANTS

=============================================================================
*/



#define VIEWTILEX	(viewwidth/16)
#define VIEWTILEY	(viewheight/16)

#define MAX_WARP_LEVEL	23

/*
=============================================================================

						 GLOBAL VARIABLES

=============================================================================
*/

boolean ForceLoadDefault=false;

int DebugKeys (void);
/*
=============================================================================

						 LOCAL VARIABLES

=============================================================================
*/

boolean PP_step=false;

// line 229


//===========================================================================

/*
==================
=
= CountObjects
=
==================
*/

void CountObjects (void)
{
	int	i,total,count,active,inactive,doors;
	objtype	*obj;

	CenterWindow (16,7);
	active = inactive = count = doors = 0;

	US_Print ("Total statics :");
	total = laststatobj-&statobjlist[0];
	US_PrintUnsigned (total);

	US_Print ("\nIn use statics:");
	for (i=0;i<total;i++)
		if (statobjlist[i].shapenum != -1)
			count++;
		else
			doors++;	//debug
	US_PrintUnsigned (count);

	US_Print ("\nDoors         :");
	US_PrintUnsigned (doornum);

	for (obj=player->next;obj;obj=obj->next)
	{
		if (obj->active)
			active++;
		else
			inactive++;
	}

	US_Print ("\nTotal actors  :");
	US_PrintUnsigned (active+inactive);

	US_Print ("\nActive actors :");
	US_PrintUnsigned (active);

	VW_UpdateScreen();
	IN_Ack ();
}


//------------------------------------------------------------------------
// CountTotals
//------------------------------------------------------------------------
void CountTotals(void)
{
	CenterWindow (20,11);

   US_Print ("  CURRENT MAP TOTALS\n");

	US_Print ("\nTotal Enemy:\n");
	US_PrintUnsigned (gamestuff.level[gamestate.mapon].stats.total_enemy);

	US_Print ("\nTotal Points:\n");
	US_PrintUnsigned (gamestuff.level[gamestate.mapon].stats.total_points);

	US_Print ("\nTotal Informants:\n");
	US_PrintUnsigned (gamestuff.level[gamestate.mapon].stats.total_inf);

	VW_UpdateScreen();
	IN_Ack ();
}

//------------------------------------------------------------------------
// ShowMap()
//------------------------------------------------------------------------
void ShowMap(void)
{
   objtype old_player;

   memcpy(&old_player,player,sizeof(objtype));
	player->angle = 90;
	player->x = player->y = ((long)32<<TILESHIFT)+(TILEGLOBAL/2);

	CenterWindow (20,11);

   US_CPrint("CURRENT MAP\n\n ");

	ShowOverhead(160-32,py,32,0,OV_ACTORS|OV_SHOWALL|OV_KEYS|OV_PUSHWALLS);
   VW_UpdateScreen();

   memcpy(player,&old_player,sizeof(objtype));
	IN_Ack ();
}

//===========================================================================

//===========================================================================

// line 333

// line 519

//===========================================================================


//---------------------------------------------------------------------------
// IncRange - Incs a value to a MAX value (including max value)
//
// NOTE: Assumes that 0 is the lowest value
//---------------------------------------------------------------------------
unsigned IncRange(unsigned Value,unsigned MaxValue)
{
	if (Value == MaxValue)
   	Value = 0;
   else
   	Value++;

   return (Value);
}

//---------------------------------------------------------------------------
// DecRange - Decs a value to 0 and resets to MAX_VALUE
//
// NOTE: Assumes that 0 is the lowest value
//---------------------------------------------------------------------------
unsigned DecRange(unsigned Value,unsigned MaxValue)
{
	if (Value == 0)
		Value = MaxValue;
	else
		Value--;

	return (Value);
}


/*
================
=
= DebugKeys
=
================
*/

#if IN_DEVELOPMENT
char far TestAutoMapperMsg[] = {"AUTOMAPPER TEST\n ENTER COUNT:"};
char far TestQuickSaveMsg[] = {"QUICK SAVE TEST\n ENTER COUNT:"};
#endif


int DebugKeys (void)
{
	char str[3];
	boolean esc;
	int level,i;
    
    if (Keyboard[sc_A]) {		// A = Show Actors on AutoMap
   	    ExtraRadarFlags ^= OV_ACTORS;
		CenterWindow (24,3);
		if (ExtraRadarFlags & OV_ACTORS)
		  US_PrintCentered ("AUTOMAP: Show Actors ON");
		else
		  US_PrintCentered ("AUTOMAP: Show Actors OFF");
		VW_UpdateScreen();
		IN_Ack();
		return 1;
    }
    else if (Keyboard[sc_B]) {		// B = border color
		CenterWindow(24,3);
		PrintY+=6;
		US_Print(" Border color (0-15):");
		VW_UpdateScreen();
		esc = !US_LineInput (px,py,str,NULL,true,2,0);
		if (!esc)
		{
			level = atoi (str);
            // TODO: Find a healthier alternative
			//if (level>=0 && level<=15)
				//DrawStatusBorder (level);
		}
		return 1;
	}

	if (Keyboard[sc_K])		// K = Map Content totals
	{
		CountTotals();
		return 1;
	}
	else if (Keyboard[sc_C])		// C = count objects
	{
		CountObjects();
		return 1;
	}
	else if (Keyboard[sc_R])		// C = count objects
	{
		ShowMap();
		return 1;
	}
	else if (Keyboard[sc_D]) {			// D = Dumb/Blind Objects (Player Invisable)
		CenterWindow (19,3);
		PlayerInvisable ^= 1;
		if (PlayerInvisable)
			US_PrintCentered ("Player Invisible!");
		else
			US_PrintCentered ("Player visible");

		VW_UpdateScreen();
		IN_Ack ();
		return 1;
	}
	else if (Keyboard[sc_E]) {			// E = Win Mission
        CenterWindow (19,3);
        US_PrintCentered ("Instant Wiener!");
        InstantWin = 1;
        playstate = ex_victorious;
        VW_UpdateScreen();
        IN_Ack ();
        return 1;
	}
	else if (Keyboard[sc_F]) {		// F = facing spot
		CenterWindow (18,5);
		US_Print ("X:");
		US_PrintUnsigned (player->x);
		US_Print ("  ");
		US_PrintUnsigned (player->x>>TILESHIFT);
		US_Print ("\nY:");
		US_PrintUnsigned (player->y);
		US_Print ("  ");
		US_PrintUnsigned (player->y>>TILESHIFT);
		US_Print ("\nA:");
		US_PrintUnsigned (player->angle);
		US_Print ("\nD:");
		US_PrintUnsigned (player->dir);
		VW_UpdateScreen();
		IN_Ack();
		return 1;
	}

	if (Keyboard[sc_G])		// G = god mode
	{
		CenterWindow (12,2);
		if (godmode)
		  US_PrintCentered ("God mode OFF");
		else
		  US_PrintCentered ("God mode ON");
		VW_UpdateScreen();
		IN_Ack();
		godmode ^= 1;
		return 1;
	}


	if (Keyboard[sc_H])		// H = hurt self
	{
		IN_ClearKeysDown ();
		TakeDamage (1,NULL);
	}
	else
	if (Keyboard[sc_I])			// I = item cheat
	{
		char loop;
		CenterWindow (12,3);
		US_PrintCentered ("Free items!");
		VW_UpdateScreen();
		HealSelf (99);
		GiveToken(5);

		for (i=wp_autocharge;i<=wp_bfg_cannon;i++)
			if (!(gamestate.weapons & (1<<i)))
			{
				GiveWeapon (i);
				break;
			}

		gamestate.ammo += 50;
		if (gamestate.ammo > MAX_AMMO)
			gamestate.ammo = MAX_AMMO;
		DrawAmmo(true);
        DrawScore();
		IN_Ack ();
		return 1;
	}
#if IN_DEVELOPMENT
#if (!BETA_TEST)
	else if (Keyboard[sc_N])			// N = no clip
	{
		gamestate.flags ^= GS_CLIP_WALLS;
		CenterWindow (18,3);
		if (gamestate.flags & GS_CLIP_WALLS)
			US_PrintCentered ("NO clipping OFF");
		else
			US_PrintCentered ("No clipping ON");
		VW_UpdateScreen();
		IN_Ack ();
		return 1;
	}
#endif
	else if (Keyboard[sc_P])			// P = pause with no screen disruptioon
	{
		PicturePause ();
		return 1;
	}
#endif
	else if (Keyboard[sc_Q])			// Q = fast quit
		Quit (NULL);
#if IN_DEVELOPMENT
	else if (Keyboard[sc_T])			// T = shape test
	{
		ShapeTest ();
		return 1;
	}
#endif
	else if (Keyboard[sc_O])			// O = Show Push Walls
    {
   	    ExtraRadarFlags ^= OV_PUSHWALLS;
		CenterWindow (24,3);
		if (ExtraRadarFlags & OV_PUSHWALLS)
		    US_PrintCentered ("AUTOMAP: Show PWalls ON");
		else
		    US_PrintCentered ("AUTOMAP: Show PWalls OFF");
		VW_UpdateScreen();
		IN_Ack();
   	    return 1;
    }
	else if (Keyboard[sc_U])			// Unlock All Floors
	{
   	    int i;
		CenterWindow (24,3);
	  	US_PrintCentered ("Unlock All Floors!");
		VW_UpdateScreen();
		IN_Ack();

		for (i=0;i<11;i++)
			gamestuff.level[i].locked=false;

      return 1;
	}
	else if (Keyboard[sc_V])			// V = extra VBLs
	{
		CenterWindow(30,3);
		PrintY+=6;
		US_Print("  Add how many extra VBLs(0-8):");
		VW_UpdateScreen();
		esc = !US_LineInput (px,py,str,NULL,true,2,0);
		if (!esc)
		{
			level = atoi (str);
			if (level>=0 && level<=8)
				extravbls = level;
		}
		return 1;
	}
	else
	if (Keyboard[sc_S])			// S = slow motion
	{
		singlestep^=1;
		CenterWindow (18,3);
		if (singlestep)
			US_PrintCentered ("Slow motion ON");
		else
			US_PrintCentered ("Slow motion OFF");
		VW_UpdateScreen();
		IN_Ack ();
		return 1;
	}
	else if (Keyboard[sc_W])			// W = warp to level
	{
		ForceLoadDefault=Keyboard[sc_LShift]|Keyboard[sc_RShift]|Keyboard[sc_CapsLock];

		CenterWindow(26,5);
		PrintY+=6;
		if (ForceLoadDefault)
			US_Print("         --- LOAD DEFAULT ---\n");
		US_Print("  Current map: ");
		US_PrintUnsigned(gamestate.mapon);
		US_Print("\n  Enter map number: ");
		VW_UpdateScreen();
		esc = !US_LineInput (px,py,str,NULL,true,2,0);
		if (!esc)
		{
			level = atoi (str);
			if (level>-1 && level<=MAX_WARP_LEVEL)
			{
				gamestate.lastmapon = gamestate.mapon;
				gamestate.mapon = level-1;
				playstate = ex_warped;
				if (ForceLoadDefault)
					BONUS_QUEUE = BONUS_SHOWN = 0;
			}
		}
		return 1;
	}
	else if (Keyboard[sc_Home])		// Dec top color
	{
#ifdef CEILING_FLOOR_COLORS
		if (gamestate.flags & GS_DRAW_CEILING)
#endif
		{
			CeilingTile = DecRange(CeilingTile,NUM_TILES-1);
			SetPlaneViewSize ();     // Init new textures
			return(1);
		}
#ifdef CEILING_FLOOR_COLORS
		else
		{
			TopColor = DecRange((TopColor&0xff),0xff);
			TopColor |= (TopColor<<8);
		}
#endif
	}
	else if (Keyboard[sc_PgUp])		// Inc top color
	{
#ifdef CEILING_FLOOR_COLORS
		if (gamestate.flags & GS_DRAW_CEILING)
#endif
		{
			CeilingTile = IncRange(CeilingTile,NUM_TILES-1);
			SetPlaneViewSize ();     // Init new textures
			return(1);
		}
#ifdef CEILING_FLOOR_COLORS
		else
		{
			TopColor = IncRange((TopColor&0xff),0xff);
			TopColor |= (TopColor<<8);
		}
#endif
	}
	else if (Keyboard[sc_End])			// Dec bottom color
	{
#ifdef CEILING_FLOOR_COLORS
		if (gamestate.flags & GS_DRAW_FLOOR)
#endif
		{
			FloorTile = DecRange(FloorTile,NUM_TILES-1);
			SetPlaneViewSize ();     // Init new textures
			return(1);
		}
#ifdef CEILING_FLOOR_COLORS
		else
		{
			BottomColor = DecRange((BottomColor&0xff),0xff);
			BottomColor |= (BottomColor<<8);
		}
#endif
	}
	else if (Keyboard[sc_PgDn])		// Inc bottom color
	{
#ifdef CEILING_FLOOR_COLORS
		if (gamestate.flags & GS_DRAW_FLOOR)
#endif
		{
			FloorTile = IncRange(FloorTile,NUM_TILES-1);
			SetPlaneViewSize ();     // Init new textures
			return(1);
		}
#ifdef CEILING_FLOOR_COLORS
		else
		{
			BottomColor = IncRange((BottomColor&0xff),0xff);
			BottomColor |= (BottomColor<<8);
		}
#endif
	}

#if (IN_DEVELOPMENT)
#if !BETA_TEST
	else if (Keyboard[sc_Y])
	{
		GivePoints(100000L,false);
	}
#endif
#endif

	if (gamestate.flags & GS_LIGHTING)			// Shading adjustments
	{
   	if (Keyboard[sc_Plus] && normalshade_div < 12)
      	normalshade_div++;
      else
   	if (Keyboard[sc_Minus] && normalshade_div>1)
      	normalshade_div--;

		normalshade=(3*(maxscale>>2))/normalshade_div;

   	if (Keyboard[sc_RBrace] && shade_max < 63)
      	shade_max++;
      else
   	if (Keyboard[sc_LBrace] && shade_max > 5)
      	shade_max--;
	}

	return 0;
}

#if 0
/*
===================
=
= OverheadRefresh
=
===================
*/

void OverheadRefresh (void)
{
	unsigned	x,y,endx,endy,sx,sy;
	unsigned	tile;


	endx = maporgx+VIEWTILEX;
	endy = maporgy+VIEWTILEY;

	for (y=maporgy;y<endy;y++)
		for (x=maporgx;x<endx;x++)
		{
			sx = (x-maporgx)*16;
			sy = (y-maporgy)*16;

			switch (viewtype)
			{
#if 0
			case mapview:
				tile = *(mapsegs[0]+farmapylookup[y]+x);
				break;

			case tilemapview:
				tile = tilemap[x][y];
				break;

			case visview:
				tile = spotvis[x][y];
				break;
#endif
			case actoratview:
				tile = (unsigned)actorat[x][y];
				break;
			}

			if (tile<MAXWALLTILES)
				LatchDrawTile(sx,sy,tile);
			else
			{
				LatchDrawChar(sx,sy,NUMBERCHARS+((tile&0xf000)>>12));
				LatchDrawChar(sx+8,sy,NUMBERCHARS+((tile&0x0f00)>>8));
				LatchDrawChar(sx,sy+8,NUMBERCHARS+((tile&0x00f0)>>4));
				LatchDrawChar(sx+8,sy+8,NUMBERCHARS+(tile&0x000f));
			}
		}

}


/*
===================
=
= ViewMap
=
===================
*/

void ViewMap (void)
{
	boolean		button0held;

	viewtype = actoratview;
//	button0held = false;


	maporgx = player->tilex - VIEWTILEX/2;
	if (maporgx<0)
		maporgx = 0;
	if (maporgx>MAPSIZE-VIEWTILEX)
		maporgx=MAPSIZE-VIEWTILEX;
	maporgy = player->tiley - VIEWTILEY/2;
	if (maporgy<0)
		maporgy = 0;
	if (maporgy>MAPSIZE-VIEWTILEY)
		maporgy=MAPSIZE-VIEWTILEY;

	do
	{
//
// let user pan around
//
		PollControls ();
		if (controlx < 0 && maporgx>0)
			maporgx--;
		if (controlx > 0 && maporgx<mapwidth-VIEWTILEX)
			maporgx++;
		if (controly < 0 && maporgy>0)
			maporgy--;
		if (controly > 0 && maporgy<mapheight-VIEWTILEY)
			maporgy++;

#if 0
		if (c.button0 && !button0held)
		{
			button0held = true;
			viewtype++;
			if (viewtype>visview)
				viewtype = mapview;
		}
		if (!c.button0)
			button0held = false;
#endif

		OverheadRefresh ();

	} while (!Keyboard[sc_Escape]);

	IN_ClearKeysDown ();
}
#endif



#if IN_DEVELOPMENT
//-------------------------------------------------------------------------
// CalcMemFree()
//-------------------------------------------------------------------------
void CalcMemFree(void)
{
	__PUR_MEM_AVAIL__ = MM_TotalFree();
	__FREE_MEM_AVAIL__ = MM_UnusedMemory();
}
#endif
