// WOLFHACK.C

#include "3d_def.h"

#define	MAXVIEWHEIGHT	8192
#define  GAMESTATE_TEST 	1


unsigned CeilingTile=126, FloorTile=126;

void (*MapRowPtr)();

static int		spanstart[MAXVIEWHEIGHT/2];		// jtr - far

static fixed	stepscale[MAXVIEWHEIGHT/2];
static fixed	basedist[MAXVIEWHEIGHT/2];

static byte	planepics[8192];	// 4k of ceiling, 4k of floor

static int	halfheight = 0;

static unsigned	planeylookup[MAXVIEWHEIGHT/2];
static unsigned	mirrorofs[MAXVIEWHEIGHT/2];

static unsigned mr_rowofs;
static unsigned mr_count;
static int16_t	mr_xstep, mr_ystep, mr_xfrac, mr_yfrac;
static byte*	mr_dest;

extern byte * shadingtable;
extern byte * vbuf;
extern unsigned vbufPitch;

void MapLSRow()
{
	unsigned int delta = mr_rowofs;
	unsigned int count = mr_count;
	const unsigned int step = (uint16_t(mr_ystep)<<16)|(uint16_t(mr_xstep));
	unsigned int position = (uint16_t(mr_yfrac)<<16)|(uint16_t(mr_xfrac));
	byte* dest = mr_dest;
	const byte* const src = planepics;

	unsigned int offs;
	do
	{
		offs = (((position>>3)&0x1F80)|(position>>25))&0x1FFE;
		position += step;
		*dest = shadingtable[src[offs]]; // write ceiling pixel
		dest[delta] = shadingtable[src[offs+1]]; // write floor pixel
		dest += 4;
	}
	while(--count);
}

void F_MapLSRow()
{
	unsigned int count = mr_count;
	const unsigned int step = (uint16_t(mr_ystep)<<16)|(uint16_t(mr_xstep));
	unsigned int position = (uint16_t(mr_yfrac)<<16)|(uint16_t(mr_xfrac));
	byte* dest = mr_dest + mr_rowofs;
	const byte* const src = planepics+1;

	unsigned int offs;
	do
	{
		offs = (((position>>3)&0x1F80)|(position>>25))&0x1FFE;
		position += step;
		*dest = shadingtable[src[offs]]; // write floor pixel
		dest += 4;
	}
	while(--count);
}

void C_MapLSRow()
{
	unsigned int count = mr_count;
	const unsigned int step = (uint16_t(mr_ystep)<<16)|(uint16_t(mr_xstep));
	unsigned int position = (uint16_t(mr_yfrac)<<16)|(uint16_t(mr_xfrac));
	byte* dest = mr_dest;
	const byte* const src = planepics;

	unsigned int offs;
	do
	{
		offs = (((position>>3)&0x1F80)|(position>>25))&0x1FFE;
		position += step;
		*dest = shadingtable[src[offs]]; // write ceiling pixel
		dest += 4;
	}
	while(--count);
}

void MapRow()
{
	unsigned int delta = mr_rowofs;
	unsigned int count = mr_count;
	const unsigned int step = (uint16_t(mr_ystep)<<16)|(uint16_t(mr_xstep));
	unsigned int position = (uint16_t(mr_yfrac)<<16)|(uint16_t(mr_xfrac));
	byte* dest = mr_dest;
	const byte* const src = planepics;

	unsigned int offs;
	do
	{
		offs = (((position>>3)&0x1F80)|(position>>25))&0x1FFE;
		position += step;
		*dest = src[offs]; // write ceiling pixel
		dest[delta] = src[offs+1]; // write floor pixel
		dest += 4;
	}
	while(--count);
}

void F_MapRow()
{
	unsigned int count = mr_count;
	const unsigned int step = (uint16_t(mr_ystep)<<16)|(uint16_t(mr_xstep));
	unsigned int position = (uint16_t(mr_yfrac)<<16)|(uint16_t(mr_xfrac));
	byte* dest = mr_dest + mr_rowofs;
	const byte* const src = planepics+1;

	unsigned int offs;
	do
	{
		offs = (((position>>3)&0x1F80)|(position>>25))&0x1FFE;
		position += step;
		*dest = src[offs]; // write floor pixel
		dest += 4;
	}
	while(--count);
}

void C_MapRow()
{
	unsigned int count = mr_count;
	const unsigned int step = (uint16_t(mr_ystep)<<16)|(uint16_t(mr_xstep));
	unsigned int position = (uint16_t(mr_yfrac)<<16)|(uint16_t(mr_xfrac));
	byte* dest = mr_dest;
	const byte* const src = planepics;

	unsigned int offs;
	do
	{
		offs = (((position>>3)&0x1F80)|(position>>25))&0x1FFE;
		position += step;
		*dest = src[offs]; // write ceiling pixel
		dest += 4;
	}
	while(--count);
}

/*
==============
=
= DrawSpans
=
= Height ranges from 0 (infinity) to viewheight/2 (nearest)
==============
*/

void DrawSpans (int x1, int x2, int height)
{
	fixed		length;
	int			ofs;
	int			prestep;
	fixed		startxfrac, startyfrac;

	int			x, startx, count, plane, startplane;
	byte		*toprow, *dest;
	long i;

	toprow = vbuf+planeylookup[height];
	mr_rowofs = mirrorofs[height];

	mr_xstep = (viewsin<<1)/height;
	mr_ystep = (viewcos<<1)/height;

	length = basedist[height];
	startxfrac = (viewx + FixedMul(length,viewcos));
	startyfrac = (viewy - FixedMul(length,viewsin));

// draw two spans simultaniously

	if (gamestate.flags & GS_LIGHTING)
	{

		i=shade_max-(63l*(unsigned)height/(unsigned)normalshade);
		if (i<0)
			i=0;
		else if (i>63)
			i = 63;
		shadingtable=lightsource[i];
		plane = startplane = x1&3;
		prestep = viewwidth/2 - x1;
		do
		{
			mr_xfrac = startxfrac - (mr_xstep>>2)*prestep;
			mr_yfrac = startyfrac - (mr_ystep>>2)*prestep;

			startx = x1>>2;
			mr_dest = toprow + (startx<<2) + plane;
			mr_count = ((x2-plane)>>2) - startx + 1;
			x1++;
			prestep--;

#if GAMESTATE_TEST
			if (mr_count)
				MapRowPtr();
#else
			if (mr_count)
				MapLSRow ();
#endif

			plane = (plane+1)&3;
		}
		while (plane != startplane);
	}
	else
	{
		plane = startplane = x1&3;
		prestep = viewwidth/2 - x1;
		do
		{
			mr_xfrac = startxfrac - (mr_xstep>>2)*prestep;
			mr_yfrac = startyfrac - (mr_ystep>>2)*prestep;

			startx = x1>>2;
			mr_dest = toprow + (startx<<2) + plane;
			mr_count = ((x2-plane)>>2) - startx + 1;
			x1++;
			prestep--;

#if GAMESTATE_TEST
			if (mr_count)
				MapRowPtr();
#else
			if (mr_count)
				MapRow ();
#endif

			plane = (plane+1)&3;
		}
		while (plane != startplane);
	}
}




/*
===================
=
= SetPlaneViewSize
=
===================
*/

void SetPlaneViewSize (void)
{
	int		x,y;
	byte 	*dest, *src;

	halfheight = viewheight>>1;


	for (y=0 ; y<halfheight ; y++)
	{
		planeylookup[y] = (halfheight-1-y)*vbufPitch;
		mirrorofs[y] = (y*2+1)*vbufPitch;

		stepscale[y] = y*GLOBAL1/32;
		if (y>0)
			basedist[y] = GLOBAL1/2*scale/y;
	}


	src = PM_GetPage(CeilingTile);
	dest = planepics;
	for (x=0 ; x<4096 ; x++)
	{
		*dest = *src++;
		dest += 2;
	}
	src = PM_GetPage(FloorTile);
	dest = planepics+1;
	for (x=0 ; x<4096 ; x++)
	{
		*dest = *src++;
		dest += 2;
	}

}


/*
===================
=
= DrawPlanes
=
===================
*/

void DrawPlanes (void)
{
	int		height, lastheight;
	int		x;

#if IN_DEVELOPMENT
	if (!MapRowPtr)
		DRAW2_ERROR(NULL_FUNC_PTR_PASSED);
#endif


	if (viewheight>>1 != halfheight)
		SetPlaneViewSize ();		// screen size has changed

//
// loop over all columns
//
 	lastheight = halfheight;

	for (x=0 ; x<viewwidth ; x++)
	{
		height = wallheight[x]>>3;
		if (height < lastheight)
		{
			// more starts
			do
			{
				spanstart[--lastheight] = x;
			}
			while (lastheight > height);
		}
		else if (height > lastheight)
		{
			// draw spans
			if (height > halfheight)
				height = halfheight;
			for ( ; lastheight < height ; lastheight++)
				if (lastheight>0)
					DrawSpans (spanstart[lastheight], x-1, lastheight);
		}
	}

	height = halfheight;
	for ( ; lastheight < height ; lastheight++)
		if (lastheight>0)
			DrawSpans (spanstart[lastheight], x-1, lastheight);
}
