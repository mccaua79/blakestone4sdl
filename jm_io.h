#include "3d_def.h"

//-------------------------------------------------------------------------
// Function Prototypes
//-------------------------------------------------------------------------
//boolean IO_FarRead (int handle, byte *dest, long length);
//boolean IO_FarWrite (int handle, byte *source, long length);
//boolean IO_ReadFile (char *filename, memptr *ptr);
//boolean IO_WriteFile(char *filename, void *ptr, long length);
//long IO_LoadFile (char *filename, memptr *dst);
void IO_CopyFile(char *sFilename, char *dFilename);
void IO_CopyHandle(int sHandle, int dHandle, long num_bytes);
