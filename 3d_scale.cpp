// 3D_SCALE.C

#include "3d_def.h"
#pragma hdrstop

#define OP_RETF	0xcb

#define CLOAKED_SHAPES			(true)

/*
=============================================================================

						  GLOBALS

=============================================================================
*/

//t_compscale _seg *scaledirectory[MAXSCALEHEIGHT+1];
//long			fullscalefarcall[MAXSCALEHEIGHT+1];

int			maxscale,maxscaleshl2;
unsigned    centery;

int normalshade;
int normalshade_div = 1;
int shade_max = 1;

int nsd_table[] = { 1, 6, 3, 4, 1, 2};
int sm_table[] =  {36,51,62,63,18,52};


/*
=============================================================================

						  LOCALS

=============================================================================
*/

//t_compscale 	_seg *work;
unsigned BuildCompScale (int height, memptr *finalspot);

int			stepbytwo;

//===========================================================================

#if 0
/*
==============
=
= BadScale
=
==============
*/

void far BadScale (void)
{
	SCALE_ERROR(BADSCALE_ERROR);
}
#endif


/*
==========================
=
= SetupScaling
=
==========================
*/

void SetupScaling (int maxscaleheight)
{
	int		i,x,y;
	byte    *dest;

	maxscaleheight/=2;			// one scaler every two pixels

	maxscale = maxscaleheight-1;
	maxscaleshl2 = maxscale<<2;
	normalshade=(3*(maxscale>>2))/normalshade_div;
	centery=viewheight>>1;
}

//===========================================================================

// Line 87

// Line 202
extern boolean useBounceOffset;

fixed bounceOffset=0;

// Line 205
