#ifdef _WIN32
    #include <io.h>
#else
    #include <unistd.h>
	#define chsize(a,b) ftruncate(a,b)
	static inline off_t tell(int fd)
	{
		return lseek(fd, 0, SEEK_CUR);
	}
#endif

#include <sys/stat.h>
#include <sys/types.h>
#pragma hdrstop
#include "3d_def.h"
#include "jm_lzh.h"

/*
=============================================================================

                                            BLAKE STONE
                         (C)opyright 1993, JAM Productions, Inc.

                         3D engine licensed by ID Software, Inc.
                    Shareware distribution by Apogee Software, Inc.

=============================================================================
*/

/*
=============================================================================

                         LOCAL CONSTANTS

=============================================================================
*/

#define SKIP_TITLE_AND_CREDITS		0


#define FOCALLENGTH     (0x5700l)               // in global coordinates
#define VIEWGLOBAL      0x10000                 // globals visable flush to wall

#define VIEWWIDTH       256                     // size of view window
#define VIEWHEIGHT      144

#define MAX_DEST_PATH_LEN	30

/*
=============================================================================

                            GLOBAL VARIABLES

=============================================================================
*/

extern int pickquick;


void DrawCreditsPage(void);
void unfreed_main(void);
void ShowPromo(void);

const char* MainStrs[] = {
    "q","nowait","l","e",
    "version","system",
    "dval","tics","mem","powerball","music","d",
    "radar",BETA_CODE,
    nil
};

short starting_episode,starting_level,starting_difficulty;

char destPath[MAX_DEST_PATH_LEN+1];
char tempPath[MAX_DEST_PATH_LEN+15];

void InitPlaytemp(void);

char QuitMsg[] = {"Unit: $%02x Error: $%02x"};

#ifdef CEILING_FLOOR_COLORS
unsigned TopColor,BottomColor;
#endif


char    str[80];
int     dirangle[9] = {0,ANGLES/8,2*ANGLES/8,3*ANGLES/8,4*ANGLES/8,
                       5*ANGLES/8,6*ANGLES/8,7*ANGLES/8,ANGLES};

//
// proejection variables
//
fixed    focallength;
unsigned screenofs;
int      viewscreenx, viewscreeny;
int      viewwidth;
int      viewheight;
short    centerx;
int      shootdelta;           // pixels away from centerx a target can be
fixed    scale;
int32_t  heightnumerator;


void    Quit (const char *error,...);

boolean startgame;
boolean loadedgame;
int     mouseadjustment;

short view_xl,view_xh,view_yl,view_yh;

#if IN_DEVELOPMENT
unsigned	democount=0,jim=0;
#endif

/*
=============================================================================

                            LOCAL VARIABLES

=============================================================================
*/

#if 0

unsigned mspeed;

void CalcSpeedRating()
{
	short loop;

	for (loop=0; loop<10; loop++)
	{
		ThreeDRefresh();
		mspeed += tics;
	}
}

#endif

/*
====================
=
= WriteConfig
=
====================
*/

void WriteConfig(void)
{
    char configpath[300];

#ifdef _arch_dreamcast
    fs_unlink(configname);
#endif

    if(configdir[0])
        snprintf(configpath, sizeof(configpath), "%s/%s", configdir, configname);
    else
        strcpy(configpath, configname);

	MakeDestPath(configname);
    const int file = open(configpath, O_CREAT | O_WRONLY | O_BINARY, 0644);

	if (file != -1)
	{
		write(file,Scores,sizeof(HighScore) * MaxScores);

		write(file,&SoundMode,sizeof(SoundMode));
		write(file,&MusicMode,sizeof(MusicMode));
		write(file,&DigiMode,sizeof(DigiMode));

		write(file,&mouseenabled,sizeof(mouseenabled));
		write(file,&joystickenabled,sizeof(joystickenabled));
        // TODO: These lines could be removed (because they are unnecessary)
        boolean dummyJoypadEnabled = false;
        write(file,&dummyJoypadEnabled,sizeof(dummyJoypadEnabled));
        boolean dummyJoystickProgressive = false;
        write(file,&dummyJoystickProgressive,sizeof(dummyJoystickProgressive));
        int dummyJoystickPort = 0;
        write(file,&dummyJoystickPort,sizeof(dummyJoystickPort));
        ////

		write(file,&dirscan,sizeof(dirscan));
		write(file,&buttonscan,sizeof(buttonscan));
		write(file,&buttonmouse,sizeof(buttonmouse));
		write(file,&buttonjoy,sizeof(buttonjoy));

		write(file,&viewsize,sizeof(viewsize));
		write(file,&mouseadjustment,sizeof(mouseadjustment));

		write(file,&gamestate.flags,sizeof(gamestate.flags));		

		close(file);
	}
}


//===========================================================================

/*
=====================
=
= NewGame
=
= Set up new game to start from the beginning
=
=====================
*/

boolean ShowQuickMsg;
void NewGame (int difficulty,int episode)
{
	unsigned oldf=gamestate.flags,loop;

	InitPlaytemp();
	playstate = ex_stillplaying;

	ShowQuickMsg=true;
    memset (&gamestuff,0,sizeof(gamestuff));
	memset (&gamestate,0,sizeof(gamestate));

	memset(&gamestate.barrier_table,0xff,sizeof(gamestate.barrier_table));
	memset(&gamestate.old_barrier_table,0xff,sizeof(gamestate.old_barrier_table));
	gamestate.flags = oldf & ~(GS_KILL_INF_WARN);
//	LoadAccessCodes();

	gamestate.difficulty = difficulty;


//
// The following are set to 0 by the memset() to gamestate - Good catch! :JR
//
//	gamestate.rzoom
//	gamestate.rpower
//	gamestate.old_door_bombs
// gamestate.plasma_detonators
//

	gamestate.weapons	 = 1<<wp_autocharge;			// |1<<wp_plasma_detonators;
	gamestate.weapon = gamestate.chosenweapon = wp_autocharge;
	gamestate.old_weapons[0] = gamestate.weapons;
	gamestate.old_weapons[1] = gamestate.weapon;
	gamestate.old_weapons[2] = gamestate.chosenweapon;

	gamestate.health = 100;
	gamestate.old_ammo = gamestate.ammo = STARTAMMO;
//	gamestate.dollars = START_DOLLARS;
//	gamestate.cents   = START_CENTS;
	gamestate.lives = 3;
	gamestate.nextextra = EXTRAPOINTS;
	gamestate.episode=episode;
	gamestate.flags |= (GS_CLIP_WALLS|GS_ATTACK_INFOAREA);	//|GS_DRAW_CEILING|GS_DRAW_FLOOR);

#if IN_DEVELOPMENT || TECH_SUPPORT_VERSION
	if (gamestate.flags & GS_STARTLEVEL)
	{
		gamestate.mapon = starting_level;
		gamestate.difficulty = starting_difficulty;
		gamestate.episode = starting_episode;
	}
	else
#endif
		gamestate.mapon = 0;

	gamestate.key_floor = gamestate.mapon+1;
	startgame = true;

	for (loop=0; loop<MAPS_WITH_STATS; loop++)
	{
		gamestuff.old_levelinfo[loop].stats.overall_floor=100;
		if (loop)
			gamestuff.old_levelinfo[loop].locked=true;
	}

//	normalshade_div = SHADE_DIV;
//	shade_max = SHADE_MAX;
	ExtraRadarFlags = InstantWin = InstantQuit = 0;

	pickquick = 0;
}

//===========================================================================

//==========================================================================
//
//             'LOAD/SAVE game' and 'LOAD/SAVE level' code
//
//==========================================================================

boolean LevelInPlaytemp(char levelnum);

#define WriteIt(c,p,s)	cksize+=WriteInfo(c,(byte *)p,s,handle)
#define ReadIt(d,p,s)	ReadInfo(d,(byte *)p,s,handle)

//#define LZH_WORK_BUFFER_SIZE	8192		

//memptr lzh_work_buffer;
long checksum;

//--------------------------------------------------------------------------
// InitPlaytemp()
//--------------------------------------------------------------------------
void InitPlaytemp()
{
	int handle;
	long size;

	MakeDestPath(PLAYTEMP_FILE);
	if ((handle=open(tempPath,O_CREAT|O_TRUNC|O_RDWR|O_BINARY, 0644))==-1)
		MAIN_ERROR(INITPLAYTEMP_OPEN_ERR);

	close(handle);
}


//--------------------------------------------------------------------------
// DoChecksum()
//--------------------------------------------------------------------------
int32_t DoChecksum(byte *source,unsigned size,int32_t checksum)
{
    unsigned i;

    for (i=0;i<size-1;i++)
    checksum += source[i]^source[i+1];

    return checksum;
}

//--------------------------------------------------------------------------
// FindChunk()
//--------------------------------------------------------------------------
int32_t FindChunk(int file, const char *chunk)
{
	long chunklen;
	char fchunk[5]={0,0,0,0,0};

	while (1)
	{
		if (read(file,fchunk,4)!=4)			// read chunk id
			break;
		read(file,&chunklen,4);					// read chunk length

		if (strstr(fchunk,chunk))				// look for chunk (sub-check!)
			return(chunklen);						// chunk found!

		lseek(file,chunklen,SEEK_CUR);		// skip this chunk
	}

	lseek(file,0,SEEK_END);						// make sure we're at the end
	return(0);
}

//--------------------------------------------------------------------------
// FindChunk()
//--------------------------------------------------------------------------
#define CHUNK_READ_SIZE 4
int32_t FindChunk(FILE *file, const char *chunk)
{
	long chunklen;
	char fchunk[5]={0,0,0,0,0};

	while (1)
	{
        int rSize = fread(fchunk,CHUNK_READ_SIZE,1,file) * CHUNK_READ_SIZE; // fread returns # of elements returned, multiply by chunk size read
		if (rSize!=CHUNK_READ_SIZE)			// read chunk id
			break;
		fread(&chunklen,4,1,file);					// read chunk length

		if (strstr(fchunk,chunk))				// look for chunk (sub-check!)
			return(chunklen);						// chunk found!

		fseek(file,chunklen,SEEK_CUR);		// skip this chunk
	}

	fseek(file,0,SEEK_END);						// make sure we're at the end
	return(0);
}

//--------------------------------------------------------------------------
// NextChunk()
//--------------------------------------------------------------------------
int32_t NextChunk(int file)
{
	uint32_t chunklen;
	uint8_t fchunk[5]={0,0,0,0,0};

	if (read(file,fchunk,CHUNK_READ_SIZE) != CHUNK_READ_SIZE)			// read chunk id
	{
		lseek(file,0,SEEK_END);				// make sure we're at the end
		return(0);
	}

	read(file,&chunklen,4);					// read chunk length
	return(chunklen);
}

int32_t NextChunk(FILE* file)
{
	uint32_t chunklen;
	uint8_t fchunk[5]={0,0,0,0,0};


	if (fread(fchunk,CHUNK_READ_SIZE,1,file)*CHUNK_READ_SIZE != CHUNK_READ_SIZE)			// read chunk id
	{
		fseek(file,0,SEEK_END);				// make sure we're at the end
		return(0);
	}

	fread(&chunklen,4,1,file);					// read chunk length
	return(chunklen);
}

char LS_current=-1,LS_total=-1;

//--------------------------------------------------------------------------
// ReadInfo()
//--------------------------------------------------------------------------
void ReadInfo(boolean decompress,byte *dst, unsigned size, int file)
{
	unsigned csize,dsize;

//	PreloadUpdate(LS_current++,LS_total);

	/*if (decompress)
	{
		read(file,(byte *)&csize,sizeof(csize));
		read(file,(byte*)lzh_work_buffer,csize);
		checksum=DoChecksum((byte *)lzh_work_buffer,csize,checksum);
		dsize=LZH_Decompress(lzh_work_buffer,dst,size,csize,SRC_MEM|DEST_MEM);
		if (dsize != size)
			MAIN_ERROR(READINFO_BAD_DECOMP);
	}
	else*/
	//{
		read(file,dst,size);
		checksum=DoChecksum(dst,size,checksum);
	//}
}

//--------------------------------------------------------------------------
// WriteInfo()
//--------------------------------------------------------------------------
unsigned WriteInfo(boolean compress, byte *src, unsigned size, int handle)
{
	unsigned csize;

	//PreloadUpdate(LS_current++,LS_total);

	/*if (compress)
	{
		csize=LZH_Compress(src,lzh_work_buffer,size,SRC_MEM|DEST_MEM);
		if (csize > LZH_WORK_BUFFER_SIZE)
			MAIN_ERROR(WRITEINFO_BIGGER_BUF);
		write (handle,&csize,sizeof(csize));
		write (handle,lzh_work_buffer,csize);
		checksum=DoChecksum((byte *)lzh_work_buffer,csize,checksum);
		csize += sizeof(csize);
	}
	else*/
	//{
		write (handle,src,size);
		checksum=DoChecksum(src,size,checksum);
		csize=size;
	//}

	return(csize);
}
//--------------------------------------------------------------------------
// LoadLevel()
//--------------------------------------------------------------------------
boolean LoadLevel(short levelnum)
{
	extern boolean ShowQuickMsg;
	extern boolean ForceLoadDefault;
	extern unsigned destoff;

	boolean oldloaded=loadedgame;
	long oldchecksum;
	objtype *ob;
	statobj_t *statptr;
	int handle,picnum;
	memptr temp;
	unsigned count;
	char *ptr;
	char chunk[5]="LVxx";

extern int nsd_table[];
extern int sm_table[];

char mod;

	WindowY=181;
	gamestuff.level[levelnum].locked=false;

	mod = levelnum % 6;
	normalshade_div = nsd_table[mod];
	shade_max = sm_table[mod];
	normalshade=(3*(maxscale>>2))/normalshade_div;

// Open PLAYTEMP file
//

    MakeDestPath(PLAYTEMP_FILE);
	handle=open(tempPath,O_RDONLY|O_BINARY);

// If level exists in PLAYTEMP file, use it; otherwise, load it from scratch!
//
	sprintf(&chunk[2],"%02x",levelnum);
	if ((handle==-1) || (!FindChunk(handle,chunk)) || ForceLoadDefault)
	{
		close(handle);

		//PreloadUpdate(LS_current+((LS_total-LS_current)>>1),LS_total);
		SetupGameLevel();
		gamestate.flags |= GS_VIRGIN_LEVEL;
		gamestate.turn_around=0;

		//PreloadUpdate(1,1);
		ForceLoadDefault=false;
		goto overlay;
	}

	gamestate.flags &= ~GS_VIRGIN_LEVEL;

// Setup for LZH decompression
//
	//LZH_Startup();
    
    //lzh_work_buffer = malloc(LZH_WORK_BUFFER_SIZE);
    //CHECKMALLOCRESULT(lzh_work_buffer);

// Read all sorts of stuff...
//
    
	checksum = 0;

	loadedgame=true;
	SetupGameLevel();
	loadedgame=oldloaded;

	ReadIt(true, tilemap, sizeof(tilemap));
	ReadIt(true, actorat, sizeof(actorat));
	ReadIt(true, areaconnect, sizeof(areaconnect));
	ReadIt(true, areabyplayer, sizeof(areabyplayer));

// Restore 'save game' actors
//
	ReadIt(false, &count, sizeof(count));    
    temp = malloc(count*sizeof(*ob));
    CHECKMALLOCRESULT(temp);
	ReadIt(true, temp, count*sizeof(*ob));
	ptr=(char *)temp;

	InitActorList ();							// start with "player" actor
	memcpy(newobj,ptr,sizeof(*ob)-4);		// don't copy over links!
	ptr += sizeof(*ob);						//

	while (--count)
	{
		GetNewActor();
		memcpy(newobj,ptr,sizeof(*ob)-4);		// don't copy over links!
		actorat[newobj->tilex][newobj->tiley]=newobj;
#if LOOK_FOR_DEAD_GUYS
		if (newobj->flags & FL_DEADGUY)
			DeadGuys[NumDeadGuys++]=newobj;
#endif
		ptr += sizeof(*ob);
	}

	free(temp);


   //
	//  Re-Establish links to barrier switches
	//

#pragma warn -pia

	ob = objlist;
	do
	{
		switch (ob->obclass)
		{
			case arc_barrierobj:
			case post_barrierobj:
         case vspike_barrierobj:
         case vpost_barrierobj:
				ob->temp2 = ScanBarrierTable(ob->tilex,ob->tiley);
			break;
		}
	} while (ob = ob->next);

#pragma warn +pia

	ConnectBarriers();

// Read all sorts of stuff...
//
	ReadIt(false, &laststatobj, sizeof(laststatobj));
	ReadIt(true, statobjlist, sizeof(statobjlist));
	ReadIt(true, doorposition, sizeof(doorposition));
	ReadIt(true, doorobjlist, sizeof(doorobjlist));
	ReadIt(false, &pwallstate, sizeof(pwallstate));
	ReadIt(false, &pwallx, sizeof(pwallx));
	ReadIt(false, &pwally, sizeof(pwally));
	ReadIt(false, &pwalldir, sizeof(pwalldir));
	ReadIt(false, &pwallpos, sizeof(pwallpos));
	ReadIt(false, &pwalldist, sizeof(pwalldist));
	ReadIt(true, TravelTable, sizeof(TravelTable));
	ReadIt(true, &ConHintList, sizeof(ConHintList));
	ReadIt(true, eaList, sizeof(eaWallInfo)*MAXEAWALLS);
	ReadIt(true, &GoldsternInfo, sizeof(GoldsternInfo));
   ReadIt(true, &GoldieList,sizeof(GoldieList));			
	ReadIt(false, gamestate.barrier_table,sizeof(gamestate.barrier_table));
	ReadIt(false, &gamestate.plasma_detonators,sizeof(gamestate.plasma_detonators));

// Read and evaluate checksum
//
	//PreloadUpdate(LS_current++,LS_total);
	read (handle,(byte *)&oldchecksum,sizeof(oldchecksum));

	if (oldchecksum != checksum)
	{
		int old_wx=WindowX,old_wy=WindowY,old_ww=WindowW,old_wh=WindowH,
			 old_px=px,old_py=py;

		WindowX=0; WindowY=16; WindowW=320; WindowH=168;
		CacheMessage(BADINFO_TEXT);
		WindowX=old_wx; WindowY=old_wy; WindowW=old_ww; WindowH=old_wh;
		px=old_px; py=old_py;

		IN_ClearKeysDown();
		IN_Ack();

		gamestate.score = 0;
		gamestate.nextextra = EXTRAPOINTS;
		gamestate.lives = 1;

		gamestate.weapon = gamestate.chosenweapon = wp_autocharge;
		gamestate.weapons = 1<<wp_autocharge;		// |1<<wp_plasma_detonators;

		gamestate.ammo = 8;
	}

	close(handle);

// Clean-up LZH compression
//
	//free(lzh_work_buffer);
	//LZH_Shutdown();
	NewViewSize(viewsize);

// Check for Strange Door and Actor combos
//
	CleanUpDoors_N_Actors();
    

overlay:;
    
	return(true);
}
//--------------------------------------------------------------------------
// SaveLevel()
//--------------------------------------------------------------------------
boolean SaveLevel(short levelnum)
{
	objtype *ob;
	int handle;
	//struct ffblk finfo;
	long offset,cksize;
	char chunk[5]="LVxx";
	unsigned gflags = gamestate.flags;
	boolean rt_value=false;
	memptr temp;
	unsigned count;
	char *ptr;
	char oldmapon;

	WindowY=181;

// Make sure floor stats are saved!
//
	oldmapon=gamestate.mapon;
	gamestate.mapon=gamestate.lastmapon;
	ShowStats(0,0,ss_justcalc,&gamestuff.level[gamestate.mapon].stats);
	gamestate.mapon=oldmapon;

// Yeah! We're no longer a virgin!
//
	gamestate.flags &= ~GS_VIRGIN_LEVEL;

// Open PLAYTEMP file
//
	MakeDestPath(PLAYTEMP_FILE);
	if ((handle=open(tempPath,O_CREAT|O_RDWR|O_BINARY,0644))==-1)
		MAIN_ERROR(SAVELEVEL_DISKERR);

// Remove level chunk from file
//
	sprintf(&chunk[2],"%02x",levelnum);
	DeleteChunk(handle,chunk);

// Setup LZH compression
//
//	LZH_Startup();
//    lzh_work_buffer = malloc(LZH_WORK_BUFFER_SIZE);
 //   CHECKMALLOCRESULT(lzh_work_buffer);

// Write level chunk id
//
	write(handle,chunk,4);
	lseek(handle,4,SEEK_CUR);		// leave four bytes for chunk size

// Write all sorts of info...
//
	checksum = cksize = 0;
	WriteIt(true, tilemap, sizeof(tilemap));
	WriteIt(true, actorat, sizeof(actorat));
	WriteIt(true, areaconnect, sizeof(areaconnect));
	WriteIt(true, areabyplayer, sizeof(areabyplayer));

// Write actor list...
//
    temp = malloc(sizeof(objlist));
    CHECKMALLOCRESULT(temp);
	for (ob=player,count=0,ptr=(char *)temp; ob; ob=ob->next,count++,ptr+=sizeof(*ob))
		memcpy(ptr,ob,sizeof(*ob));
	WriteIt(false, &count, sizeof(count));
	WriteIt(true, temp, count*sizeof(*ob));
	free(temp);

// Write all sorts of info...
//
	WriteIt(false, &laststatobj, sizeof(laststatobj));
	WriteIt(true, statobjlist, sizeof(statobjlist));
	WriteIt(true, doorposition, sizeof(doorposition));
	WriteIt(true, doorobjlist, sizeof(doorobjlist));
	WriteIt(false, &pwallstate, sizeof(pwallstate));
	WriteIt(false, &pwallx, sizeof(pwallx));
	WriteIt(false, &pwally, sizeof(pwally));
	WriteIt(false, &pwalldir, sizeof(pwalldir));
	WriteIt(false, &pwallpos, sizeof(pwallpos));
	WriteIt(false, &pwalldist, sizeof(pwalldist));
	WriteIt(true, TravelTable, sizeof(TravelTable));
	WriteIt(true, &ConHintList, sizeof(ConHintList));
	WriteIt(true, eaList, sizeof(eaWallInfo)*MAXEAWALLS);
	WriteIt(true, &GoldsternInfo, sizeof(GoldsternInfo));
	WriteIt(true, &GoldieList,sizeof(GoldieList));
	WriteIt(false, gamestate.barrier_table,sizeof(gamestate.barrier_table));
	WriteIt(false, &gamestate.plasma_detonators,sizeof(gamestate.plasma_detonators));

// Write checksum and determine size of file
//
	WriteIt(false, &checksum, sizeof(checksum));
	offset=tell(handle);

// Write chunk size, set file size, and close file
//
	lseek(handle,-(cksize+4),SEEK_CUR);
	write(handle,&cksize,4);

	chsize(handle,offset);
	close(handle);
	rt_value=true;

// Clean-up LZH compression
//
exit_func:;
	//free(lzh_work_buffer);
	//LZH_Shutdown();
	NewViewSize(viewsize);
	gamestate.flags = gflags;

	return(rt_value);
}

#pragma warn -pia

//--------------------------------------------------------------------------
// DeleteChunk()
//--------------------------------------------------------------------------
long DeleteChunk(int handle, char *chunk)
{
	long filesize,cksize,offset,bmove;
	int dhandle;

	lseek(handle,0,SEEK_SET);
	filesize=lseek(handle,0,SEEK_END);
	lseek(handle,0,SEEK_SET);

	if (cksize=FindChunk(handle,chunk))
	{
		offset=lseek(handle,0,SEEK_CUR)-8; 		// move back to CKID/SIZE
		bmove=filesize-(offset+8+cksize);	 	// figure bytes to move

		if (bmove)										// any data to move?
		{
		// Move data: FROM --> the start of NEXT chunk through the end of file.
		//              TO --> the start of THIS chunk.
		//
		// (ie: erase THIS chunk and re-write it at the end of the file!)
		//
			lseek(handle,cksize,SEEK_CUR);			// seek source to NEXT chunk

			MakeDestPath(PLAYTEMP_FILE);
			if ((dhandle=open(tempPath,O_CREAT|O_RDWR|O_BINARY,0644))==-1)
				MAIN_ERROR(SAVELEVEL_DISKERR);

			lseek(dhandle,offset,SEEK_SET);  		// seek dest to THIS chunk
			IO_CopyHandle(handle,dhandle,bmove);	// copy "bmove" bytes

			close(dhandle);

			lseek(handle,offset+bmove,SEEK_SET);	// go to end of data moved
		}
		else
			lseek(handle,offset,SEEK_SET);
	}

	return(cksize);
}

#pragma warn +pia


char SavegameInfoText[]="\n\r"
									 "\n\r"
									 "-------------------------------------\n\r"
									 "    Blake Stone: Aliens Of Gold\n\r"
									 "Copyright 1993, JAM Productions, Inc.\n\r"
									 "\n\r"
									 "SAVEGAME file is from version: "__BLAKE_VERSION__"\n\r"
									 " Compile Date :"__DATE__" : "__TIME__"\n\r"
									 "-------------------------------------\n\r"
									 "\x1a";


//===========================================================================

/*
==================
=
= LoadTheGame
=
==================
*/

boolean LoadTheGame(FILE *file)
{
	extern int lastmenumusic;

	long cksize;
	memptr temp=NULL;
	boolean rt_value=false;
    char InfoSpace[400];
    memptr tempspace;

// Setup LZH decompression
//
	//LZH_Startup();
    //lzh_work_buffer = malloc(LZH_WORK_BUFFER_SIZE);

// Read in VERSion chunk
//
	if (!FindChunk(file,"VERS"))
		goto cleanup;

	cksize = sizeof(SavegameInfoText);
	fread(InfoSpace, cksize,1,file);
    // TODO: Ignore for testing
	if (false/*memcmp(InfoSpace, SavegameInfoText, cksize)*/) {
		// Old Version of game

		int old_wx=WindowX,old_wy=WindowY,old_ww=WindowW,old_wh=WindowH,
			 old_px=px,old_py=py;

		WindowX=0; WindowY=16; WindowW=320; WindowH=168;
		CacheMessage(BADSAVEGAME_TEXT);
		SD_PlaySound (NOWAYSND);
		WindowX=old_wx; WindowY=old_wy; WindowW=old_ww; WindowH=old_wh;
		px=old_px; py=old_py;

	  	IN_ClearKeysDown();
	  	IN_Ack();

        VW_FadeOut();
        screenfaded = true;

     	goto cleanup;
	}

// Read in HEAD chunk
//
	if (!FindChunk(file,"HEAD"))
		goto cleanup;

	fread(&gamestate, sizeof(gamestate),1, file);
	fread(&gamestuff, sizeof(gamestuff),1, file);

// Reinitialize page manager
//
#if DUAL_SWAP_FILES
	PM_Shutdown();
	PM_Startup ();
	PM_UnlockMainMem();
#endif


// Start music for the starting level in this loaded game.
//

	FreeMusic();
	StartMusic(false);

// Copy all remaining chunks to PLAYTEMP file
//
	MakeDestPath(PLAYTEMP_FILE);
    int sHandle;
	if ((sHandle=open(tempPath,O_CREAT|O_TRUNC|O_RDWR|O_BINARY, 0644))==-1)
		goto cleanup;

#pragma warn -pia
	while (cksize=NextChunk(file))
	{
		cksize += 8;								// include chunk ID and LENGTH
		fseek(file,-8,SEEK_CUR);				// seek to start of chunk
        temp = malloc(cksize);
		fread(temp,cksize,1, file);		// read chunk from SAVEGAME file
		write(sHandle,temp,cksize);	// write chunk to PLAYTEMP file
		free(temp);						// free temp buffer
	}
#pragma warn +pia

	close(sHandle);
	rt_value=true;

// Clean-up LZH decompression
//
cleanup:;
	//free(lzh_work_buffer);
	//LZH_Shutdown();
	NewViewSize(viewsize);

// Load current level
//
	if (rt_value)
	{
		LoadLevel(0xff);
		ShowQuickMsg=false;
	}

	return(rt_value);
}

boolean SaveTheGame(FILE *file, char *description)
{
    struct stat statbuf;
	uint32_t cksize,offset;
	int shandle;
	memptr temp;
	char nbuff[GAME_DESCRIPTION_LEN+1];
	boolean rt_value=false,exists;
//
// Save PLAYTEMP becuase we'll want to restore it to the way it was
// before the save.
//
//	IO_CopyFile(PLAYTEMP_FILE,OLD_PLAYTEMP_FILE);
//

// Save current level -- saves it into PLAYTEMP.
//
	SaveLevel(0xff);
// Setup LZH compression
//
	//LZH_Startup();
    //lzh_work_buffer = malloc(LZH_WORK_BUFFER_SIZE);

// Write VERSion chunk
//
	cksize=sizeof(SavegameInfoText);
	fwrite("VERS",4,1,file);
	fwrite(&cksize,4, 1, file);
	fwrite(SavegameInfoText,cksize, 1, file);

// Write DESC chunk
//
	memcpy(nbuff,description,sizeof(nbuff));
	cksize=strlen(nbuff)+1;
	fwrite("DESC",4, 1, file);
	fwrite(&cksize,4, 1, file);
	fwrite(nbuff,cksize, 1, file);

// Write HEAD chunk
//
	cksize=0;
	fwrite("HEAD",4,1,file);

	fseek(file,4,SEEK_CUR);		// leave four bytes for chunk size
    
    cksize += sizeof(gamestate);
	fwrite(&gamestate, sizeof(gamestate),1, file);
    cksize += sizeof(gamestuff);
	fwrite(&gamestuff, sizeof(gamestuff),1, file);

    // Appends size of gamestate/gamestuff into head, then skips back to after gamestate/gamestuff
	fseek(file,-(cksize+4),SEEK_CUR);
	fwrite(&cksize,4, 1, file);
	fseek(file,cksize,SEEK_CUR);

	{
	// Append PLAYTEMP file to savegame file
	//
		MakeDestPath(PLAYTEMP_FILE);
		if (stat(tempPath, &statbuf) != 0)
			goto cleanup;
		
		FILE *fTemp = fopen (tempPath, "rb");
		if (fTemp == NULL)
			goto cleanup;

		// copy to savegame file
		fseek(fTemp,0, SEEK_END); // goes to end
		uint32_t size = ftell(fTemp); // checks size
		memptr tempDst = malloc(size); // allocate size
		CHECKMALLOCRESULT(tempDst);
		fseek(fTemp,0,SEEK_SET); // go back to the beginning
		fread(tempDst,size,1,fTemp); // read whole file
		fwrite(tempDst, size, 1, file); // save byte array to savegame file

		fclose(fTemp);
		rt_value=true;
	}

// Clean-up LZH compression
//
cleanup:;
	//free(lzh_work_buffer);
	//LZH_Shutdown();
	NewViewSize(viewsize);

//
// Return PLAYTEMP to original state!
//
//	remove(PLAYTEMP_FILE);
//	rename(OLD_PLAYTEMP_FILE,PLAYTEMP_FILE);
//

	return(rt_value);
}
//--------------------------------------------------------------------------
// LevelInPlaytemp()
//--------------------------------------------------------------------------
boolean LevelInPlaytemp(char levelnum)
{
	int handle;
	char chunk[5]="LVxx";
	boolean rt_value;

// Open PLAYTEMP file
//
	MakeDestPath(PLAYTEMP_FILE);
	handle=open(tempPath,O_RDONLY|O_BINARY);

// See if level exists in PLAYTEMP file...
//
    // NOTE: Added this to avoid loading a non-existant file
    if (handle != -1) {
	    sprintf(&chunk[2],"%02x",levelnum);
	    rt_value=FindChunk(handle,chunk);
// Close PLAYTEMP file
//
	    close(handle);
	    return(rt_value);
    } else {
        return false;
    }
}

// Line 1013

// CheckDiskSpace (is it needed?)
// Line 1066

//--------------------------------------------------------------------------
// CleanUpDoors_N_Actors()
//--------------------------------------------------------------------------
void CleanUpDoors_N_Actors(void)
{
	/*char x,y;
   objtype *obj;
   objtype **actor_ptr;
   byte *tile_ptr;
	unsigned door;

   actor_ptr = (objtype **)actorat;
	tile_ptr = (byte *)tilemap;

   for (y=0;y<mapheight;y++)
	   for (x=0;x<mapwidth;x++)
      {
      	if (*tile_ptr & 0x80)
         {
         	// Found a door
            //

            obj = *actor_ptr;
            if ((obj >= objlist) && (obj < &objlist[MAXACTORS]))
            {
             	// Found an actor

            	// Determine door number...

	         	door = *tile_ptr & 0x3F;

					if ((obj->flags & (FL_SOLID|FL_DEADGUY)) == (FL_SOLID|FL_DEADGUY))
   	         	obj->flags &= ~(FL_SHOOTABLE | FL_SOLID | FL_FAKE_STATIC);

					// Make sure door is open

					doorobjlist[door].ticcount = 0;
					doorobjlist[door].action = dr_open;
					doorposition[door] = 0xffff;
            }
         }

         tile_ptr++;
      	actor_ptr++;
      }*/
}


//--------------------------------------------------------------------------
// ClearNClose() - Use when changing levels via standard elevator.
//
//               - This code doesn't CLEAR the elevator door as originally
//                 planned because, actors were coded to stay out of the
//                 elevator doorway.
//
//--------------------------------------------------------------------------
void ClearNClose()
{
	/*char x,y,tx=0,ty=0,px=player->x>>TILESHIFT,py=player->y>>TILESHIFT;

	// Locate the door.
	//
	for (x=-1; x<2 && !tx; x+=2)
		for (y=-1; y<2; y+=2)
			if (tilemap[px+x][py+y] & 0x80)
			{
				tx=px+x;
				ty=py+y;
				break;
			}

	// Close the door!
	//
	if (tx)
	{
		char doornum=tilemap[tx][ty]&63;

		doorobjlist[doornum].action = dr_closed;		// this door is closed!
		doorposition[doornum]=0;							// draw it closed!
		(unsigned)actorat[tx][ty] = doornum | 0x80;	// make it solid!
	}*/
}

//--------------------------------------------------------------------------
// CycleColors()
//--------------------------------------------------------------------------
void CycleColors()
{
	#define NUM_RANGES 	5
	#define CRNG_LOW		0xf0
	#define CRNG_HIGH		0xfe
	#define CRNG_SIZE		(CRNG_HIGH-CRNG_LOW+1)

	static CycleInfo crng[NUM_RANGES] = {{7,0,0xf0,0xf1},
													 {15,0,0xf2,0xf3},
													 {30,0,0xf4,0xf5},
													 {10,0,0xf6,0xf9},
													 {12,0,0xfa,0xfe},
													};

	byte loop,cbuffer[CRNG_SIZE][3];
	boolean changes=false;
	for (loop=0; loop<NUM_RANGES; loop++)
	{
		CycleInfo *c=&crng[loop];

		if (tics >= c->delay_count)
		{
			byte temp[3],first,last,numregs;

			if (!changes)
			{
                SDL_Color pal[256];
				VL_GetPalette(pal);
                memcpy(cbuffer, &pal[CRNG_LOW],CRNG_SIZE);
				changes=true;
			}

			first = c->firstreg-CRNG_LOW;
			numregs = c->lastreg-c->firstreg;	// is one less than in range
			last = first+numregs;

			memcpy(temp,cbuffer[last],3);
			memmove(cbuffer[first+1],cbuffer[first],numregs*3);
			memcpy(cbuffer[first],temp,3);

			c->delay_count = c->init_delay;
		}
		else
			c->delay_count -= tics;
	}

	if (changes) {
        SDL_Color pal[256];
		VL_GetPalette(pal);
        memcpy(cbuffer, &pal[CRNG_LOW],CRNG_SIZE);
    }
	else
		VW_WaitVBL(1);
}


/*
==========================
=
= ShutdownId
=
= Shuts down all ID_?? managers
=
==========================
*/

void ShutdownId (void)
{
    US_Shutdown ();         // This line is completely useless...
    SD_Shutdown ();
    PM_Shutdown ();
    IN_Shutdown ();
    VW_Shutdown ();
    CA_Shutdown ();
#if defined(GP2X_940)
    GP2X_Shutdown();
#endif
}

//===========================================================================


/*
====================
=
= CalcProjection
=
= Uses focallength
=
====================
*/

void CalcProjection (int32_t focal)
{
    int     i;
    int    intang;
    float   angle;
    double  tang;
    int     halfview;
    double  facedist;

    focallength = focal;
    facedist = focal+MINDIST;
    halfview = viewwidth/2;                                 // half view in pixels

    //
    // calculate scale value for vertical height calculations
    // and sprite x calculations
    //
    scale = (fixed) (halfview*facedist/(VIEWGLOBAL/2));

    //
    // divide heightnumerator by a posts distance to get the posts height for
    // the heightbuffer.  The pixel height is height>>2
    //
    heightnumerator = (TILEGLOBAL*scale)>>6;

    //
    // calculate the angle offset from view angle of each pixel's ray
    //

    for (i=0;i<halfview;i++)
    {
        // start 1/2 pixel over, so viewangle bisects two middle pixels
        tang = (int32_t)i*VIEWGLOBAL/viewwidth/facedist;
        angle = (float) atan(tang);
        intang = (int) (angle*radtoint);
        pixelangle[halfview-1-i] = intang;
        pixelangle[halfview+i] = -intang;
    }
}



//===========================================================================

//--------------------------------------------------------------------------
// DoMovie()
//--------------------------------------------------------------------------
boolean DoMovie(movie_t movie, memptr palette)
{
    boolean  ReturnVal;
	StopMusic();
    SD_StopSound();

    ClearMemory();
    UnCacheLump(STARTFONT,STARTFONT+NUMFONT);
    CA_LoadAllSounds();

    // This is not working right (needs a full palette copy
    if (palette) {
        memcpy(Movies[movie].palette, palette, sizeof(SDL_Color) * 256);
    }
    else {
        memcpy(Movies[movie].palette, gamepal, sizeof(SDL_Color) * 256);
    }

    ReturnVal = MOVIE_Play(&Movies[movie]);

    SD_StopSound();
    ClearMemory();
    LoadFonts();

    return(ReturnVal);
}


//===========================================================================

/*
=================
=
= MS_CheckParm
=
=================
*/
// Line 1323

// Line: 1353
//===========================================================================

//--------------------------------------------------------------------------
// LoadFonts()
//--------------------------------------------------------------------------
void LoadFonts(void)
{
    CA_CacheGrChunk(STARTFONT+4);

    CA_CacheGrChunk(STARTFONT+2);
}



//===========================================================================
// CalcProjection

//===========================================================================

/*
==========================
=
= SetViewSize
=
==========================
*/

boolean SetViewSize (unsigned width, unsigned height)
{
    viewwidth = width&~15;                  // must be divisable by 16
    viewheight = height&~1;                 // must be even
    centerx = viewwidth/2-1;
    shootdelta = viewwidth/10;
    
    viewscreenx = (screenWidth-viewwidth) / 2;
    viewscreeny = (screenHeight-scaleFactor*STATUSLINES-viewheight+TOP_STRIP_HEIGHT*scaleFactor)/2;
	screenofs = viewscreeny*screenWidth+viewscreenx;//((200-STATUSLINES-viewheight+TOP_STRIP_HEIGHT)/2*SCREENWIDTH+(320-viewwidth)/8);

//
// calculate trace angles and projection constants
//
    CalcProjection (FOCALLENGTH);
    
//
// build all needed compiled scalers
//
	SetupScaling (viewwidth*1.5);

	view_xl=0;
	view_xh=view_xl+viewwidth-1;
	view_yl=0;
	view_yh=view_yl+viewheight-1;

	return true;
}

void ShowViewSize (int width)
{
    int oldwidth,oldheight;

    oldwidth = viewwidth;
    oldheight = viewheight;
    
	viewwidth = (width*16)*screenWidth/320;
	viewheight = (width*16*HEIGHTRATIO)*screenHeight/200;
    
	VWB_Bar (0,TOP_STRIP_HEIGHT,320,200-STATUSLINES-TOP_STRIP_HEIGHT,BORDER_MED_COLOR);
//	VWB_Bar (0,0,320,200-STATUSLINES,BORDER_MED_COLOR);
	DrawPlayBorder ();

	viewheight = oldheight;
	viewwidth = oldwidth;
}


void NewViewSize (int width)
{
	viewsize = width;
	while (1)
	{
		if (SetViewSize (width*16*screenWidth/320,width*16*HEIGHTRATIO*screenHeight/200))
			break;
		width--;
	};
}

void Quit (const char *errorStr,...) {
#ifdef NOTYET
    byte *screen;
#endif
    char error[256];
    if(errorStr != NULL)
    {
        va_list vlist;
        va_start(vlist, errorStr);
        vsprintf(error, errorStr, vlist);
        va_end(vlist);
    }
    else error[0] = 0;
    
    if (!pictable)  // don't try to display the red box before it's loaded
    {
        ShutdownId();
        if (error && *error)
        {
#ifdef NOTYET
            SetTextCursor(0,0);
#endif
            puts(error);
#ifdef NOTYET
            SetTextCursor(0,2);
#endif
            VW_WaitVBL(100);
        }
        exit(1);
    }

    if (!error || !*error)
    {
#ifdef NOTYET
        #ifndef JAPAN
        CA_CacheGrChunk (ORDERSCREEN);
        screen = grsegs[ORDERSCREEN];
        #endif
#endif
        WriteConfig ();
    }
#ifdef NOTYET
    else
    {
        CA_CacheGrChunk (ERRORSCREEN);
        screen = grsegs[ERRORSCREEN];
    }
#endif
    
    ShutdownId ();

    if (error && *error)
    {
#ifdef NOTYET
        memcpy((byte *)0xb8000,screen+7,7*160);
        SetTextCursor(9,3);
#endif
        puts(error);
#ifdef NOTYET
        SetTextCursor(0,7);
#endif
        VW_WaitVBL(200);
        exit(1);
    }
    else
    if (!error || !(*error))
    {
#ifdef NOTYET
        #ifndef JAPAN
        memcpy((byte *)0xb8000,screen+7,24*160); // 24 for SPEAR/UPLOAD compatibility
        #endif
        SetTextCursor(0,23);
#endif
    }
    exit(0);
}

//===========================================================================

#define IFARG(str) if(!strcmp(arg, (str)))

void CheckParameters(int argc, char *argv[])
{
    bool hasError = false, showHelp = false;
    bool sampleRateGiven = false, audioBufferGiven = false;
    int defaultSampleRate = param_samplerate;

    for(int i = 1; i < argc; i++)
    {
        char *arg = argv[i];
#ifndef SPEAR
        IFARG("--goobers")
#else
        IFARG("--debugmode")
#endif
            param_debugmode = true;
        else IFARG("--baby")
            param_difficulty = 0;
        else IFARG("--easy")
            param_difficulty = 1;
        else IFARG("--normal")
            param_difficulty = 2;
        else IFARG("--hard")
            param_difficulty = 3;
        else IFARG("--nowait")
            param_nowait = true;
        else IFARG("--tedlevel")
        {
            if(++i >= argc)
            {
                printf("The tedlevel option is missing the level argument!\n");
                hasError = true;
            }
            else param_tedlevel = atoi(argv[i]);
        }
        else IFARG("--windowed")
            fullscreen = false;
        else IFARG("--windowed-mouse")
        {
            fullscreen = false;
            forcegrabmouse = true;
        }
        else IFARG("--res")
        {
            if(i + 2 >= argc)
            {
                printf("The res option needs the width and/or the height argument!\n");
                hasError = true;
            }
            else
            {
                screenWidth = atoi(argv[++i]);
                screenHeight = atoi(argv[++i]);
                unsigned factor = screenWidth / 320;
                if(screenWidth % 320 || screenHeight != 200 * factor && screenHeight != 240 * factor)
                    printf("Screen size must be a multiple of 320x200 or 320x240!\n"), hasError = true;
            }
        }
        else IFARG("--resf")
        {
            if(i + 2 >= argc)
            {
                printf("The resf option needs the width and/or the height argument!\n");
                hasError = true;
            }
            else
            {
                screenWidth = atoi(argv[++i]);
                screenHeight = atoi(argv[++i]);
                if(screenWidth < 320)
                    printf("Screen width must be at least 320!\n"), hasError = true;
                if(screenHeight < 200)
                    printf("Screen height must be at least 200!\n"), hasError = true;
            }
        }
        else IFARG("--bits")
        {
            if(++i >= argc)
            {
                printf("The bits option is missing the color depth argument!\n");
                hasError = true;
            }
            else
            {
                screenBits = atoi(argv[i]);
                switch(screenBits)
                {
                    case 8:
                    case 16:
                    case 24:
                    case 32:
                        break;

                    default:
                        printf("Screen color depth must be 8, 16, 24, or 32!\n");
                        hasError = true;
                        break;
                }
            }
        }
        else IFARG("--nodblbuf")
            usedoublebuffering = false;
        else IFARG("--extravbls")
        {
            if(++i >= argc)
            {
                printf("The extravbls option is missing the vbls argument!\n");
                hasError = true;
            }
            else
            {
                extravbls = atoi(argv[i]);
                if(extravbls < 0)
                {
                    printf("Extravbls must be positive!\n");
                    hasError = true;
                }
            }
        }
        else IFARG("--joystick")
        {
            if(++i >= argc)
            {
                printf("The joystick option is missing the index argument!\n");
                hasError = true;
            }
            else param_joystickindex = atoi(argv[i]);   // index is checked in InitGame
        }
        else IFARG("--joystickhat")
        {
            if(++i >= argc)
            {
                printf("The joystickhat option is missing the index argument!\n");
                hasError = true;
            }
            else param_joystickhat = atoi(argv[i]);
        }
        else IFARG("--samplerate")
        {
            if(++i >= argc)
            {
                printf("The samplerate option is missing the rate argument!\n");
                hasError = true;
            }
            else param_samplerate = atoi(argv[i]);
            sampleRateGiven = true;
        }
        else IFARG("--audiobuffer")
        {
            if(++i >= argc)
            {
                printf("The audiobuffer option is missing the size argument!\n");
                hasError = true;
            }
            else param_audiobuffer = atoi(argv[i]);
            audioBufferGiven = true;
        }
        else IFARG("--mission")
        {
            if(++i >= argc)
            {
                printf("The mission option is missing the mission argument!\n");
                hasError = true;
            }
            else
            {
                param_mission = atoi(argv[i]);
                if(param_mission < 0 || param_mission > 3)
                {
                    printf("The mission option must be between 0 and 3!\n");
                    hasError = true;
                }
            }
        }
        else IFARG("--configdir")
        {
            if(++i >= argc)
            {
                printf("The configdir option is missing the dir argument!\n");
                hasError = true;
            }
            else
            {
                size_t len = strlen(argv[i]);
                if(len + 2 > sizeof(configdir))
                {
                    printf("The config directory is too long!\n");
                    hasError = true;
                }
                else
                {
                    strcpy(configdir, argv[i]);
                    if(argv[i][len] != '/' && argv[i][len] != '\\')
                        strcat(configdir, "/");
                }
            }
        }
        else IFARG("--goodtimes")
            param_goodtimes = true;
        else IFARG("--ignorenumchunks")
            param_ignorenumchunks = true;
        else IFARG("--help")
            showHelp = true;
        else hasError = true;
    }
    if(hasError || showHelp)
    {
        if(hasError) printf("\n");
        printf(
            "Wolf4SDL v1.7 ($Revision: 257 $)\n"
            "Ported by Chaos-Software (http://www.chaos-software.de.vu)\n"
            "Original Wolfenstein 3D by id Software\n\n"
            "Usage: Wolf4SDL [options]\n"
            "Options:\n"
            " --help                 This help page\n"
            " --tedlevel <level>     Starts the game in the given level\n"
            " --baby                 Sets the difficulty to baby for tedlevel\n"
            " --easy                 Sets the difficulty to easy for tedlevel\n"
            " --normal               Sets the difficulty to normal for tedlevel\n"
            " --hard                 Sets the difficulty to hard for tedlevel\n"
            " --nowait               Skips intro screens\n"
            " --windowed[-mouse]     Starts the game in a window [and grabs mouse]\n"
            " --res <width> <height> Sets the screen resolution\n"
            "                        (must be multiple of 320x200 or 320x240)\n"
            " --resf <w> <h>         Sets any screen resolution >= 320x200\n"
            "                        (which may result in graphic errors)\n"
            " --bits <b>             Sets the screen color depth\n"
            "                        (use this when you have palette/fading problems\n"
            "                        allowed: 8, 16, 24, 32, default: \"best\" depth)\n"
            " --nodblbuf             Don't use SDL's double buffering\n"
            " --extravbls <vbls>     Sets a delay after each frame, which may help to\n"
            "                        reduce flickering (unit is currently 8 ms, default: 0)\n"
            " --joystick <index>     Use the index-th joystick if available\n"
            "                        (-1 to disable joystick, default: 0)\n"
            " --joystickhat <index>  Enables movement with the given coolie hat\n"
            " --samplerate <rate>    Sets the sound sample rate (given in Hz, default: %i)\n"
            " --audiobuffer <size>   Sets the size of the audio buffer (-> sound latency)\n"
            "                        (given in bytes, default: 2048 / (44100 / samplerate))\n"
            " --ignorenumchunks      Ignores the number of chunks in VGAHEAD.*\n"
            "                        (may be useful for some broken mods)\n"
            " --configdir <dir>      Directory where config file and save games are stored\n"
#if defined(_arch_dreamcast) || defined(_WIN32)
            "                        (default: current directory)\n"
#else
            "                        (default: $HOME/.wolf4sdl)\n"
#endif
#if defined(SPEAR) && !defined(SPEARDEMO)
            " --mission <mission>    Mission number to play (0-3)\n"
            "                        (default: 0 -> .sod, 1-3 -> .sd*)\n"
            " --goodtimes            Disable copy protection quiz\n"
#endif
            , defaultSampleRate
        );
        exit(1);
    }

    if(sampleRateGiven && !audioBufferGiven)
        param_audiobuffer = 2048 / (44100 / param_samplerate);
}


// Line 1559
//===========================================================================

/*
=====================
=
= DemoLoop
=
=====================
*/

void    DemoLoop (void)
{
	int     i,level;
	int 	LastDemo=0;
	boolean breakit;
	unsigned old_bufferofs;

	while (1)
	{
		playstate = ex_title;
		if (!screenfaded)
			VW_FadeOut();
		VL_SetPaletteIntensity(0,255, gamepal,0);

		while (!(gamestate.flags & GS_NOWAIT || param_nowait))
		{
			extern boolean sqActive;

		// Start music when coming from menu...
		//
			if (!sqActive)
			{
			// Load and start music
			//
				CA_CacheAudioChunk(STARTMUSIC+TITLE_LOOP_MUSIC);
				SD_StartMusic(STARTMUSIC+TITLE_LOOP_MUSIC);
			}

//
// title page
//
#if !SKIP_TITLE_AND_CREDITS
			breakit = false;
            
            SDL_Color pal[256];
			CA_CacheGrChunk(TITLEPALETTE);
            VL_ConvertPalette(grsegs[TITLEPALETTE], pal ,256);

			VW_Bar(0,0,320,200,0);
			VL_SetPalette (pal, true);
			VL_SetPaletteIntensity(0,255,pal,0);
			CA_CacheScreen(TITLE1PIC);

			fontnumber = 2;
			PrintX = WindowX = 270;
			PrintY = WindowY = 179;
			WindowW = 29;
			WindowH = 8;
			VWB_Bar(WindowX,WindowY-1,WindowW,WindowH,VERSION_TEXT_BKCOLOR);
			SETFONTCOLOR(VERSION_TEXT_COLOR, VERSION_TEXT_BKCOLOR);
			US_Print(__BLAKE_VERSION__);

			VW_UpdateScreen();
			VL_FadeIn(0,255,pal,30);
			UNCACHEGRCHUNK(TITLEPALETTE);
			if (IN_UserInput(TickBase*6))
				breakit= true;

		// Cache screen 2 with Warnings and Copyrights

			CA_CacheScreen(TITLE2PIC);
			fontnumber = 2;
			PrintX = WindowX = 270;
			PrintY = WindowY = 179;
			WindowW = 29;
			WindowH = 8;
			VWB_Bar(WindowX,WindowY-1,WindowW,WindowH,VERSION_TEXT_BKCOLOR);
			SETFONTCOLOR(VERSION_TEXT_COLOR, VERSION_TEXT_BKCOLOR);
			US_Print(__BLAKE_VERSION__);

			// Fizzle whole screen incase of any last minute changes needed
			// on title intro.

			FizzleFade(screenBuffer, 0, 0,screenWidth,screenHeight,70,false);

			IN_UserInput(TickBase*2);
			if (breakit || IN_UserInput(TickBase*6))
				break;
			VW_FadeOut();

//
// credits page
//
			DrawCreditsPage();
			VW_UpdateScreen();
			VW_FadeIn();
			if (IN_UserInput(TickBase*6))
				break;
			VW_FadeOut();

#endif

//
// demo
//

#if DEMOS_ENABLED
#if IN_DEVELOPMENT
		if (!MS_CheckParm("recdemo"))
#endif
			PlayDemo(LastDemo++%6);

			if (playstate == ex_abort)
				break;
			else
			{
			// Start music when coming from menu...
			//
				if (!sqActive)
//				if (!SD_MusicPlaying())
				{
				// Load and start music
				//
					CA_CacheAudioChunk(STARTMUSIC+TITLE_LOOP_MUSIC);
					SD_StartMusic((MusicGroup far *)audiosegs[STARTMUSIC+TITLE_LOOP_MUSIC]);
				}
			}
#endif

//
// high scores
//
#if !SKIP_TITLE_AND_CREDITS
			CA_CacheScreen (BACKGROUND_SCREENPIC);
			DrawHighScores ();
			VW_UpdateScreen ();
			VW_FadeIn ();

			if (IN_UserInput(TickBase*9))
				break;
			VW_FadeOut();
#endif
		}

        FreeMusic();

		if (!screenfaded)
			VW_FadeOut();

#ifdef DEMOS_EXTERN
		if (MS_CheckParm("recdemo"))
			RecordDemo ();
		else
#endif
		{
#if IN_DEVELOPMENT || TECH_SUPPORT_VERSION
			if (gamestate.flags & GS_QUICKRUN)
			{
				ReadGameNames();
				CA_LoadAllSounds();
				NewGame(2,gamestate.episode);
				startgame = true;
			}
			else
#endif				
				US_ControlPanel (0);
		}

		if (startgame || loadedgame)
			GameLoop ();
	}
}

//-------------------------------------------------------------------------
// DrawCreditsPage()
//-------------------------------------------------------------------------
void DrawCreditsPage()
{
	PresenterInfo pi;

	CA_CacheScreen(BACKGROUND_SCREENPIC);

	memset(&pi,0,sizeof(pi));
	pi.flags = TPF_CACHE_NO_GFX;
	pi.xl=38;
	pi.yl=28;
	pi.xh=281;
	pi.yh=170;
	pi.bgcolor = 2;
	pi.ltcolor = BORDER_HI_COLOR;
	fontcolor = BORDER_TEXT_COLOR;
	pi.shcolor = pi.dkcolor = 0;
	pi.fontnumber=fontnumber;

#ifdef ID_CACHE_CREDITS
	TP_LoadScript(NULL,&pi,CREDITSTEXT);
#else
	TP_LoadScript("CREDITS.TXT",&pi,0);
#endif

	TP_Presenter(&pi);
}

/*
==========================
=
= main
=
==========================
*/

//char    *nosprtxt[] = {"nospr",nil};
#if IN_DEVELOPMENT || TECH_SUPPORT_VERSION
short starting_episode=0,starting_level=0,starting_difficulty=2;
#endif
short debug_value=0;
int main (int argc, char *argv[]) {
#if IN_DEVELOPMENT
    MakeDestPath(ERROR_LOG);
    remove(tempPath);
#endif
    MakeDestPath(PLAYTEMP_FILE);
    remove(tempPath);

    CheckParameters(argc, argv);
    
    freed_main(argc, argv);

#if FREE_FUNCTIONS
    UseFunc((char *)JM_FREE_START,(char huge *)JM_FREE_END);
    UseFunc((char *)JM_FREE_DATA_START,(char huge *)JM_FREE_DATA_END);
#endif

    DemoLoop();

    Quit(NULL);

    return 1;
}

#if FREE_FUNCTIONS

//-------------------------------------------------------------------------
// UseFunc()
//-------------------------------------------------------------------------
unsigned UseFunc(char huge *first, char huge *next)
{
	unsigned start,end;
	unsigned pars;

	first += 15;
	next++;
	next--;

	start = FP_SEG(first);
	end = FP_SEG(next);
	if (!FP_OFF(next))
		end--;
	pars = end - start - 1;
	_fmemset(MK_FP(start,0),0,pars*16);
	MML_UseSpace(start,pars);

	return(pars);
}

#endif


//-------------------------------------------------------------------------
// fprint()
//-------------------------------------------------------------------------
void fprint(char *text)
{
	while (*text)
		printf("%c",*text++);
}


//-------------------------------------------------------------------------
// InitDestPath()
//-------------------------------------------------------------------------
void InitDestPath(void)
{
	//char *ptr;
#pragma warn -pia
	// TODO: Apogee CD stuff? Is it necessary? Really doubt it
    /*if (ptr=getenv("APOGEECD"))
	{
        struct dirent *dp;
        struct stat statbuf;
		short len;

		len = strlen(ptr);
		if (len > MAX_DEST_PATH_LEN)
		{
			printf("\nAPOGEECD path too long.\n");
			exit(0);
		}

		strcpy(destPath,ptr);
		if (destPath[len-1] == '\\')
			destPath[len-1]=0;

		if (!stat(destPath,&statbuf))
		{
			printf("\nAPOGEECD directory not found.\n");
			exit(0);
		}

		strcat(destPath,"\\");
	}
	else*/
		strcpy(destPath,"");
#pragma warn +pia
}

//-------------------------------------------------------------------------
// MakeDestPath()
//-------------------------------------------------------------------------
void MakeDestPath(const char *file)
{
    strcpy(tempPath,destPath);
    strcat(tempPath,file);
}
#if IN_DEVELOPMENT

//-------------------------------------------------------------------------
// ShowMemory()
//-------------------------------------------------------------------------
void ShowMemory(void)
{
	long psize,size;

	size = MM_TotalFree();
	psize = MM_LargestAvail();
	mprintf("Mem free: %ld   %ld\n",size,psize);
}

#endif
