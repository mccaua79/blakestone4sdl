// jm_free.cpp

//
// Warning!
//
// All functions in this source file are designated usable by the memory
// manager after program initialization.
//

#include <sys/stat.h>
#include <sys/types.h>
#ifdef _WIN32
    #include <io.h>
    #include <direct.h>
#else
    #include <unistd.h>
#endif

#include "3d_def.h"
#include <SDL_syswm.h>

#pragma hdrstop

char    configdir[256] = "";
char    configname[13] = "config.";

// TODO: Find a way to not have to store this here
//static char SaveName[13]="savegam?.";

//
// Command line parameter variables
//
boolean param_debugmode = false;
boolean param_nowait = false;
int     param_difficulty = 1;           // default is "normal"
int     param_tedlevel = -1;            // default is not to start a level
int     param_joystickindex = 0;

#if defined(_arch_dreamcast)
int     param_joystickhat = 0;
int     param_samplerate = 11025;       // higher samplerates result in "out of memory"
int     param_audiobuffer = 4096 / (44100 / param_samplerate);
#elif defined(GP2X_940)
int     param_joystickhat = -1;
int     param_samplerate = 11025;       // higher samplerates result in "out of memory"
int     param_audiobuffer = 128;
#else
int     param_joystickhat = -1;
int     param_samplerate = 44100;
int     param_audiobuffer = 2048 / (44100 / param_samplerate);
#endif

int     param_mission = 0;
boolean param_goodtimes = false;
boolean param_ignorenumchunks = false;

#define SKIP_CHECKSUMS					1
#define SHOW_CHECKSUM					0

#if GAME_VERSION == SHAREWARE_VERSION

#define AUDIOT_CHECKSUM			0xFFF87142
#define MAPTEMP_CHECKSUM		0x000370C9
#define VGAGRAPH_CHECKSUM		0xFFFFDE44
#define DIZFILE_CHECKSUM		0x00000879l

#elif GAME_VERSION == MISSIONS_1_THR_3

#define AUDIOT_CHECKSUM			0xFFF87142
#define MAPTEMP_CHECKSUM		0x00084F1F
#define VGAGRAPH_CHECKSUM		0xFFFFDE44
#define DIZFILE_CHECKSUM		0x00000879l

#else


#define AUDIOT_CHECKSUM       0xfff912C9
#define MAPTEMP_CHECKSUM		0x00107739
#define VGAGRAPH_CHECKSUM		0xffff6C9A
#define DIZFILE_CHECKSUM		0x00000879l


#endif

#pragma warn -pro
#pragma warn -use

void SDL_SBSetDMA(byte channel);
void SDL_SetupDigi(void);

//=========================================================================
//
//								 FAR FREE DATA
//
//=========================================================================

#if FREE_DATA

char JM_FREE_DATA_START[1]={0};

#endif

#if TECH_SUPPORT_VERSION
char EnterBetaCode[]="\n  TECH SUPPORT VERSION!\n\n  NO DISTRIBUTING!";
#elif BETA_TEST
char EnterBetaCode[]="      !BETA VERSION!\n    DO NOT DISTRIBUTE\n UNDER PENALTY OF DEATH\n\n   ENTER BETA CODE";
#endif


const char * JHParmStrings[] = {"no386","is386",nil};

const char show_text1[]="\n     SYSTEM INFO\n";
const char show_text2[]="=======================\n\n";
const char show_text3[]="-- Memory avail after game is loaded --\n\n";
const char show_text4[]="            ** Insufficient memory to run the game **";
const char show_text5[]="---- Extra Devices ----\n\n";

static const char * ParmStrings[] = {"HIDDENCARD",""};

// TODO: Map correctly
// channel mapping:
//  -1: any non reserved channel
//   0: player weapons
//   1: boss weapons
static int wolfdigimap[] = {
            // These first sounds are in the upload version

            ATKIONCANNONSND,					0,-1,
            ATKCHARGEDSND,						1,-1,
            ATKBURSTRIFLESND,             2,-1,
            ATKGRENADESND,                46,-1,

            OPENDOORSND,                  3,-1,
            CLOSEDOORSND,                 4,-1,
            HTECHDOOROPENSND,             5,-1,
            HTECHDOORCLOSESND,            6,-1,

            INFORMANTDEATHSND,            7,-1,
            SCIENTISTHALTSND,             19,-1,
            SCIENTISTDEATHSND,            20,-1,

            GOLDSTERNHALTSND,             8,-1,
            GOLDSTERNLAUGHSND,            24,-1,

            HALTSND,                      9,	-1,		// Rent-A-Cop 1st sighting
            RENTDEATH1SND,                10,	-1,	// Rent-A-Cop Death

            EXPLODE1SND,                  11,-1,

            GGUARDHALTSND,                12,-1,
            GGUARDDEATHSND,               17,-1,

            PROHALTSND,                   16,-1,
            PROGUARDDEATHSND,             13,-1,

            BLUEBOYDEATHSND,              18,-1,
            BLUEBOYHALTSND,					51,-1,

            SWATHALTSND,                  22,-1,
            SWATDIESND,                   47,-1,

            SCANHALTSND,                  15,-1,
            SCANDEATHSND,                 23,-1,

            PODHATCHSND,                  26,-1,
            PODHALTSND,							50,-1,
            PODDEATHSND,                  49,-1,

            ELECTSHOTSND,                 27,-1,

            DOGBOYHALTSND,                14,-1,
            DOGBOYDEATHSND,               21,-1,
            ELECARCDAMAGESND,             25,-1,
            ELECAPPEARSND,                28,-1,
            ELECDIESND,                   29,-1,

            INFORMDEATH2SND,              39,	-1,	// Informant Death #2
            RENTDEATH2SND,                34,   -1,   // Rent-A-Cop Death #2
            PRODEATH2SND,                 42,	-1,   // PRO Death #2
            SWATDEATH2SND,                48, -1,		// SWAT Death #2
            SCIDEATH2SND,					   53,-1,		// Gen. Sci Death #2

            LIQUIDDIESND,                 30,-1,

            GURNEYSND,                    31,-1,
            GURNEYDEATHSND,               41,-1,

            WARPINSND,                    32,-1,
            WARPOUTSND,                   33,-1,

            EXPLODE2SND,                  35,-1,

            LCANHALTSND,                  36,-1,
            LCANDEATHSND,                 37,-1,

//			RENTDEATH3SND,		            38,	-1,	// Rent-A-Cop Death #3
            INFORMDEATH3SND,              40,	-1,	// Informant Death #3
//			PRODEATH3SND,                 43,   -1,   // PRO Death #3
//			SWATDEATH3SND,						52,-1,		// Swat Guard #3
            SCIDEATH3SND,						54,-1,		// Gen. Sci Death #3

            LCANBREAKSND,                 44,-1,
            SCANBREAKSND,                 45,-1,
            CLAWATTACKSND,						56,-1,
            SPITATTACKSND,						55,-1,
            PUNCHATTACKSND,					57,-1,

            LASTSOUND
};


#if 0

static int wolfdigimap[] =
{
            // These first sounds are in the upload version

            ATKIONCANNONSND,					0,-1,
            ATKCHARGEDSND,						1,-1,
            ATKBURSTRIFLESND,             2,-1,
            ATKGRENADESND,                46,-1,

            OPENDOORSND,                  3,-1,
            CLOSEDOORSND,                 4,-1,
            HTECHDOOROPENSND,             5,-1,
            HTECHDOORCLOSESND,            6,-1,

            INFORMANTDEATHSND,            7,-1,
            SCIENTISTHALTSND,             19,-1,
            SCIENTISTDEATHSND,            20,-1,

            GOLDSTERNHALTSND,             8,-1,
            GOLDSTERNLAUGHSND,            24,-1,

            HALTSND,                      9,	-1,	// Rent-A-Cop 1st sighting
            RENTDEATH1SND,                10,-1,		// Rent-A-Cop Death

            EXPLODE1SND,                  11,-1,

            GGUARDHALTSND,                12,-1,
            GGUARDDEATHSND,               17,-1,

            PROHALTSND,                   16,-1,
            PROGUARDDEATHSND,             13,-1,

            BLUEBOYDEATHSND,              18,-1,
            BLUEBOYHALTSND,					51,-1,

            SWATHALTSND,                  22,-1,
            SWATDIESND,                   47,-1,

            SCANHALTSND,                  15,-1,
            SCANDEATHSND,                 23,-1,

            PODHATCHSND,                  26,-1,
            PODHALTSND,							50,-1,
            PODDEATHSND,                  49,-1,

            ELECTSHOTSND,                 27,-1,

            DOGBOYHALTSND,                14,-1,
            DOGBOYDEATHSND,               21,-1,
            ELECARCDAMAGESND,             25,-1,
            ELECAPPEARSND,                28,-1,
            ELECDIESND,                   29,-1,

            INFORMDEATH2SND,              39,-1,		// Informant Death #2
            RENTDEATH2SND,                34,-1,      // Rent-A-Cop Death #2
            PRODEATH2SND,                 42,-1,	   // PRO Death #2
            SWATDEATH2SND,                48, -1,		// SWAT Death #2
            SCIDEATH2SND,					   53,-1,		// Gen. Sci Death #2

            LIQUIDDIESND,                 30,-1,

            GURNEYSND,                    31,-1,
            GURNEYDEATHSND,               41,-1,

            WARPINSND,                    32,-1,
            WARPOUTSND,                   33,-1,

            EXPLODE2SND,                  35,-1,

            LCANHALTSND,                  36,-1,
            LCANDEATHSND,                 37,-1,

//			RENTDEATH3SND,		            38,-1,		// Rent-A-Cop Death #3
            INFORMDEATH3SND,              40,	-1,	// Informant Death #3
//			PRODEATH3SND,                 43, -1,     // PRO Death #3
//			SWATDEATH3SND,						52,-1,		// Swat Guard #3
            SCIDEATH3SND,						54,-1,		// Gen. Sci Death #3

            LCANBREAKSND,                 44,-1,
            SCANBREAKSND,                 45,-1,
            CLAWATTACKSND,						56,-1,
            SPITATTACKSND,						55,-1,
            PUNCHATTACKSND,					57,-1,

            LASTSOUND
};

#endif

//===========================================================================

/*
==================
=
= BuildTables
=
= Calculates:
=
= scale                 projection constant
= sintable/costable     overlapping fractional tables
=
==================
*/

const float radtoint = (float)(FINEANGLES/2/PI);

void BuildTables (void)
{
    //
    // calculate fine tangents
    //

    int i;
    for(i=0;i<FINEANGLES/8;i++)
    {
        double tang=tan((i+0.5)/radtoint);
        finetangent[i]=(int32_t)(tang*GLOBAL1);
        finetangent[FINEANGLES/4-1-i]=(int32_t)((1/tang)*GLOBAL1);
    }

    //
    // costable overlays sintable with a quarter phase shift
    // ANGLES is assumed to be divisable by four
    //

    float angle=0;
    float anglestep=(float)(PI/2/ANGLEQUAD);
    for(i=0; i<ANGLEQUAD; i++)
    {
        fixed value=(int32_t)(GLOBAL1*sin(angle));
        sintable[i]=sintable[i+ANGLES]=sintable[ANGLES/2-i]=value;
        sintable[ANGLES-i]=sintable[ANGLES/2+i]=-value;
        angle+=anglestep;
    }
    sintable[ANGLEQUAD] = 65536;
    sintable[3*ANGLEQUAD] = -65536;

#if defined(USE_STARSKY) || defined(USE_RAIN) || defined(USE_SNOW)
    Init3DPoints();
#endif
}

//===========================================================================

/*
===================
=
= SetupWalls
=
= Map tile values to scaled pics
=
===================
*/

void SetupWalls (void)
{
    int     i;

    horizwall[0]=0;
    vertwall[0]=0;

    for (i=1;i<MAXWALLTILES;i++)
    {
        horizwall[i]=(i-1)*2;
        vertwall[i]=(i-1)*2+1;
    }
}



/*
=====================
=
= InitDigiMap
=
=====================
*/
// 3D_GAME.C

void InitDigiMap (void)
{
    int *map;

    for (map = wolfdigimap; *map != LASTSOUND; map += 3)
    {
        DigiMap[map[0]] = map[1];
        DigiChannel[map[1]] = map[2];
        SD_PrepareSound(map[1]);
    }
}

/*
====================
=
= ReadConfig
=
====================
*/

void ReadConfig(void)
{
    SDMode  sd;
    SMMode  sm;
    SDSMode sds;

    char configpath[300];
    boolean config_found=false;
    unsigned flags=gamestate.flags;

#ifdef _arch_dreamcast
    DC_LoadFromVMU(configname);
#endif
    
    // TODO: Make this function compatible with a configdir
    //MakeDestPath(configname);
    if(configdir[0])
        snprintf(configpath, sizeof(configpath), "%s/%s", configdir, configname);
    else
        strcpy(configpath, configname);

    const int file = open(configpath, O_RDONLY | O_BINARY);
    if (file != -1)
    {
        //
        // valid config file
        //
        word tmp;
        read(file,&tmp,sizeof(tmp));
        if(tmp!=0xfefa)
        {
            close(file);
            goto noconfig;
        }
        read(file,Scores,sizeof(HighScore) * MaxScores);

        read(file,&sd,sizeof(sd));
        read(file,&sm,sizeof(sm));
        read(file,&sds,sizeof(sds));

        read(file,&mouseenabled,sizeof(mouseenabled));
        read(file,&joystickenabled,sizeof(joystickenabled));
        boolean dummyJoypadEnabled;
        read(file,&dummyJoypadEnabled,sizeof(dummyJoypadEnabled));
        boolean dummyJoystickProgressive;
        read(file,&dummyJoystickProgressive,sizeof(dummyJoystickProgressive));
        int dummyJoystickPort = 0;
        read(file,&dummyJoystickPort,sizeof(dummyJoystickPort));

        read(file,dirscan,sizeof(dirscan));
        read(file,buttonscan,sizeof(buttonscan));
        read(file,buttonmouse,sizeof(buttonmouse));
        read(file,buttonjoy,sizeof(buttonjoy));

        read(file,&viewsize,sizeof(viewsize));
        read(file,&mouseadjustment,sizeof(mouseadjustment));
        
        read(file,&flags,sizeof(flags));		// Use temp so we don't destroy pre-sets.
        flags &= GS_HEARTB_SOUND|GS_ATTACK_INFOAREA|GS_LIGHTING|GS_DRAW_CEILING|GS_DRAW_FLOOR;			 	// Mask out the useful flags!

        gamestate.flags |= flags;

        close(file);

        if ((sd == sdm_AdLib || sm == smm_AdLib) && !AdLibPresent
                && !SoundBlasterPresent)
        {
            sd = sdm_PC;
            sm = smm_Off;
        }

        if ((sds == sds_SoundBlaster && !SoundBlasterPresent))
            sds = sds_Off;

        // make sure values are correct

        if(mouseenabled) mouseenabled=true;
        if(joystickenabled) joystickenabled=true;

        if (!MousePresent)
            mouseenabled = false;
        if (!IN_JoyPresent())
            joystickenabled = false;
        
        config_found=true;

        if(mouseadjustment<0) mouseadjustment=0;
        else if(mouseadjustment>9) mouseadjustment=9;

        if(viewsize<4) viewsize=4;
        else if(viewsize>21) viewsize=21;

        MainMenu[6].active = AT_ENABLED;
        MainItems.curpos=0;
    }
    else
    {
        //
        // no config file, so select by hardware
        //
noconfig:
        if (SoundBlasterPresent || AdLibPresent)
        {
            sd = sdm_AdLib;
            sm = smm_AdLib;
        }
        else
        {
            sd = sdm_PC;
            sm = smm_Off;
        }

        if (SoundBlasterPresent)
            sds = sds_SoundBlaster;
        else
            sds = sds_Off;

        if (MousePresent)
            mouseenabled = true;

        if (IN_JoyPresent())
            joystickenabled = true;

        viewsize = 17;                          // start with a good size
        mouseadjustment=5;
        gamestate.flags |= GS_HEARTB_SOUND|GS_ATTACK_INFOAREA;

#ifdef CEILING_FLOOR_COLORS
        gamestate.flags |= GS_DRAW_CEILING|GS_DRAW_FLOOR|GS_LIGHTING;
#else
        gamestate.flags |= GS_LIGHTING;
#endif
    }

    SD_SetMusicMode (sm);
    SD_SetSoundMode (sd);
    SD_SetDigiDevice (sds);
}

#define CHECK_FOR_EPISODES

extern CP_itemtype NewEmenu[];
extern int EpisodeSelect[];

//-------------------------------------------------------------------------
// CheckForEpisodes() - CHECK FOR VERSION/EXTESION
//-------------------------------------------------------------------------
void CheckForEpisodes(void)
{
    struct stat statbuf;
    short i;

#if (GAME_VERSION != SHAREWARE_VERSION)
    if(!stat("VSWAP.VSI", &statbuf))
        strcpy (extension, "VSI");
#else
    if(!stat("*.FSW", &statbuf))
        strcpy (extension, "FSW");
#endif
    else
    {
        printf("No Fire Strike data files found!");
        exit(0);
    }

    for (i=0;i<mv_NUM_MOVIES;i++)
        strcat(Movies[i].FName,extension);

// TODO: These 2 lines
#ifdef ACTIVATE_TERMINAL
    strcat(term_com_name,extension);
    strcat(term_msg_name,extension);
#endif

    strcat(configname,extension);
    strcat(SaveName,extension);
    strcat(graphext,extension);
    strcat(audioext,extension);
    strcat(demoname,extension);
    
// TODO: These 2 lines
#if DUAL_SWAP_FILES
    strcat(AltPageFileName,extension);
    ShadowsAvail = (!findfirst(AltPageFileName,&f,FA_ARCH));
#endif
}

// TODO: BS_AOG CheckForEpisodes


extern char * MainStrs[];
extern char bc_buffer[];

//------------------------------------------------------------------------
// PreDemo()
//------------------------------------------------------------------------
void PreDemo()
{
    int i;


#if TECH_SUPPORT_VERSION

    fontnumber=4;
    SETFONTCOLOR(0,15*3);
    CenterWindow (26,7);
    US_Print(EnterBetaCode);
    VW_UpdateScreen();
    CA_LoadAllSounds();
    PM_CheckMainMem();
    SD_PlaySound(INFORMDEATH2SND);		// Nooooo!
    IN_UserInput(TickBase*20);
    ClearMemory();

#elif BETA_TEST

    boolean param=false;

    for (i=1; i<_argc; i++) {
        switch (US_CheckParm(_argv[i],MainStrs))
        {
            case 13:
                param = true;
            break;
        }
    }

    if (!param)
    {
        char buffer[15] = {0};

        fontnumber=4;
        CenterWindow (26,7);
        US_Print(EnterBetaCode);
        VW_UpdateScreen();
        SETFONTCOLOR(0,15*3);
        US_LineInput(24*8,92,buffer,buffer,true,14,100);
        if (_fstricmp(buffer,bc_buffer))
            Quit("Bad beta code!");
    }
#endif



#if GAME_VERSION == SHAREWARE_VERSION
#if IN_DEVELOPMENT || GEORGE_CHEAT
    if (!MS_CheckParm("nochex"))
#endif
    {
#if  (!SKIP_CHECKSUMS)
//	CheckValidity("MAPTEMP.",MAPTEMP_CHECKSUM,"LEVELS");
    CheckValidity("MAPTEMP.",MAPTEMP_CHECKSUM);
#endif
    }
#else
#if  (!SKIP_CHECKSUMS)
    if (ChecksumFile("FILE_ID.DIZ",0) != DIZFILE_CHECKSUM)
        gamestate.flags |= GS_BAD_DIZ_FILE;
#endif
#endif

    VL_SetPaletteIntensity(0,255,gamepal,0);

    if (!(gamestate.flags & GS_NOWAIT)) {
        if (param_nowait)
            return;
#if (0)				// GAME_VERSION != SHAREWARE_VERSION
//---------------------
// Anti-piracy screen
//---------------------
    // Cache pic
    //
        CA_CacheScreen(PIRACYPIC);

    // Cache and set palette.  AND  Fade it in!
    //
        CA_CacheGrChunk(PIRACYPALETTE);
        VL_SetPalette (0,256,grsegs[PIRACYPALETTE]);
        VL_SetPaletteIntensity(0,255,grsegs[PIRACYPALETTE],0);
        VW_UpdateScreen();

        VL_FadeOut (0, 255, 0, 0, 25, 20);
        VL_FadeIn(0,255,grsegs[PIRACYPALETTE],30);

    // Wait a little
    //
        IN_UserInput(TickBase*20);

    // Free palette
    //
        UNCACHEGRCHUNK(PIRACYPALETTE);

        VL_FadeOut (0, 255, 0, 0, 25, 20);
        VW_FadeOut();

    // Cleanup screen for upcoming SetPalette call
    //
        {
        unsigned old_bufferofs=bufferofs;

        bufferofs=displayofs;
        VL_Bar(0,0,320,200,0);
        bufferofs=old_bufferofs;
        }
#endif

//---------------------
// Apogee presents
//---------------------
    // Cache pic
    //
        CA_CacheScreen(APOGEEPIC);

    // Load and start music
    //
        CA_CacheAudioChunk(STARTMUSIC+APOGFNFM_MUS);
        SD_StartMusic(STARTMUSIC+APOGFNFM_MUS);

    // Cache and set palette.  AND  Fade it in!
    //
        CA_CacheGrChunk(APOGEEPALETTE);
        SDL_Color pal[256];
        VL_ConvertPalette (grsegs[APOGEEPALETTE], pal, 256);
        VL_SetPaletteIntensity(0,255,pal,0);
        VW_UpdateScreen();
        VL_FadeOut (0, 255, 25, 29, 53, 20);
        VL_FadeIn(0,255,pal,30);

    // Wait for end of fanfare
    //
        if (MusicMode==smm_AdLib)
        {
            IN_StartAck();
            while ((!sqPlayedOnce) && (!IN_CheckAck()));
        }
        else
            IN_UserInput(TickBase*6);

        SD_MusicOff();

    // Free palette and music.  AND  Restore palette
    //
        UNCACHEGRCHUNK(APOGEEPALETTE);
        FreeMusic();
        //free((memptr *)&audiosegs[STARTMUSIC+APOGFNFM_MUS]); // it don't like freeing the slaves

    // Do A Blue Flash!

        VL_FadeOut (0, 255, 25, 29, 53, 20);
        VL_FadeOut (0, 255, 0, 	0,  0,  30);

//---------------------
// JAM logo intro
//---------------------
    // Load and start music
    //
        CA_CacheAudioChunk(STARTMUSIC+TITLE_LOOP_MUSIC);
        SD_StartMusic(STARTMUSIC+TITLE_LOOP_MUSIC);

    // Show JAM logo
    //
        if (!DoMovie(mv_intro,0))
            MAIN_ERROR(PREDEMO_NOJAM);

        if (PowerBall) {
            int i;

            for (i=0;i<60 && (!DebugOk);i++) {
                VL_WaitVBL(1);

                if (Keyboard[sc_LShift] && Keyboard[sc_RShift]) {
                    CA_LoadAllSounds();
                    //PM_CheckMainMem();

                    SD_MusicOff();

                    SD_PlaySound(SHOOTDOORSND);
                    SD_WaitSoundDone();

                    ClearMemory();
                    DebugOk = 1;

                    CA_CacheAudioChunk(STARTMUSIC+TITLE_LOOP_MUSIC);
                    SD_StartMusic(STARTMUSIC+TITLE_LOOP_MUSIC);
                }
            }
        }

//---------------------
// PC-13
//---------------------
        VL_Bar(0,0,320,200,0x14);
        //VW_MarkUpdateBlock(0,0,319,199);
        CacheDrawPic(0,64,PC13PIC);
        VW_UpdateScreen();
        VW_FadeIn();
        IN_UserInput(TickBase*2);

        // Do A Red Flash!

        VL_FadeOut (0, 255, 39, 0, 0, 20);
        VW_FadeOut();
    }
}

//------------------------------------------------------------------------
// InitGame()
//------------------------------------------------------------------------
void InitGame (int argc, char *argv[])
{
    int                     i,x,y;
    unsigned        *blockstart;
//long mmsize;

    CA_Startup ();

// Any problems with this version of the game?
//
#if IN_DEVELOPMENT || TECH_SUPPORT_VERSION
    if (!MS_CheckParm("nochex"))
#endif

        // TODO: SKIP_CHECKSUMS gives error: invalid integer constant expression
#if !SKIP_CHECKSUMS
        CheckValidity("MAPTEMP.",MAPTEMP_CHECKSUM);

#if GAME_VERSION != SHAREWARE_VERSION
    if (ChecksumFile("FILE_ID.DIZ",0) != DIZFILE_CHECKSUM)
        gamestate.flags |= GS_BAD_DIZ_FILE;
#endif
#endif
    
    // initialize SDL
#if defined _WIN32
    putenv("SDL_VIDEODRIVER=directx");
#endif
    if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_JOYSTICK) < 0)
    {
        printf("Unable to init SDL: %s\n", SDL_GetError());
        exit(1);
    }
    atexit(SDL_Quit);

    int numJoysticks = SDL_NumJoysticks();
    if(param_joystickindex && (param_joystickindex < -1 || param_joystickindex >= numJoysticks))
    {
        if(!numJoysticks)
            printf("No joysticks are available to SDL!\n");
        else
            printf("The joystick index must be between -1 and %i!\n", numJoysticks - 1);
        exit(1);
    }

#if defined(GP2X_940)
    GP2X_MemoryInit();
#endif
    // SignonScreen(); // On Wolf3D
    
#if defined _WIN32
    if(!fullscreen)
    {
        struct SDL_SysWMinfo wmInfo;
        SDL_VERSION(&wmInfo.version);

        if(SDL_GetWMInfo(&wmInfo) != -1)
        {
            HWND hwndSDL = wmInfo.window;
            DWORD style = GetWindowLong(hwndSDL, GWL_STYLE) & ~WS_SYSMENU;
            SetWindowLong(hwndSDL, GWL_STYLE, style);
            SetWindowPos(hwndSDL, NULL, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_FRAMECHANGED);
        }
    }
#endif
    VL_SetVGAPlaneMode ();
    //VL_SetPalette (0,256,&vgapal); // set in the VGAPlaneMode (hopefully)

    VH_Startup ();
    IN_Startup ();
    PM_Startup ();
    SD_Startup ();
    US_Startup ();
    
        DebugOk = true;
    // TODO: Fix this
    /*
    if (CheckForSpecialCode(argc, argv, POWERBALLTEXT))
#if IN_DEVELOPMENT
        DebugOk = true;
#else
        PowerBall = true;
#endif

    if (CheckForSpecialCode(argc, argv, TICSTEXT))
        gamestate.flags |= GS_TICS_FOR_SCORE;

    if (CheckForSpecialCode(argc, argv, MUSICTEXT))
        gamestate.flags |= GS_MUSIC_TEST;

    if (CheckForSpecialCode(argc, argv, RADARTEXT))
        gamestate.flags |= GS_SHOW_OVERHEAD;
        */
//
// build some tables
//
    InitDigiMap ();
    
    ReadConfig ();

//
// draw intro screen stuff
//
//	if (!(gamestate.flags & GS_QUICKRUN))
//		IntroScreen ();

//
// load in and lock down some basic chunks
//

    LoadFonts();

    LoadLatchMem ();
    BuildTables ();          // trig tables
    SetupWalls ();

    NewViewSize (viewsize);

//
// initialize variables
//
    InitRedShifts ();
}

// TODO: ShowSystem

//-------------------------------------------------------------------------
// scan_atoi()
//-------------------------------------------------------------------------
unsigned scan_atoi(char *s)
{
    while (*s && (!isdigit(*s)))			// First scans for a digit...
        s++;

    return(atoi(s));							// Then converts to integer...
}

// Line: 2055

extern char * MainStrs[];
extern short starting_episode,starting_level,starting_difficulty;

//-------------------------------------------------------------------------
// freed_main()
//-------------------------------------------------------------------------
void freed_main(int argc, char *argv[])
{
    int i;

// Setup for APOGEECD thingie.
//
    InitDestPath();

// Which version is this? (SHAREWARE? 1-3? 1-6?)
//
    CheckForEpisodes();

    InitGame(argc, argv);

    PreDemo();
}
