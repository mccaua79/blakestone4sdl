#ifndef _MOVIE_H_
#define _MOVIE_H_


// TODO: This
//#include "JM_VL.H"

//==========================================================================
//
//  UNIT:  MOVIE.H
//
//==========================================================================

# pragma pack (2)
typedef struct
{
    uint16_t code; // 2 bytes
    // + 2 bytes padding
    int32_t block_num; // 4 bytes
    int32_t recsize; // 4 bytes
} anim_frame; // sizeof() == 10 (with padding 12)
# pragma pack ()

# pragma pack (push,1)
typedef struct
{
	uint16_t opt;
    uint16_t offset;
    uint16_t length;
} anim_chunk;
# pragma pack (pop)


//-------------------------------------------------------------------------
//   MovieStuff Anim Stucture...
//
//
//  fname 			-- File Name of the Anim to be played..
//	 rep				-- Number of repetitions to play the anim
//	 ticdelay		-- Tic wait between frames
//  maxmembuffer 	-- Maximum ammount to use as a ram buffer
//  start_line 	-- Starting line of screen to copy to other pages
//  end_line   	-- Ending line  "   "   "   "   "   "   "   "
//
typedef struct
{
	char FName[13];
	char rep;
	char ticdelay;

    uint32_t MaxMemBuffer;

	short start_line;
	short end_line;
    SDL_Color palette[256];
} MovieStuff_t;



//=========================================================================
//
//											EXTERNS
//
//=========================================================================

// TODO: This
//extern memptr displaybuffer;
extern MovieStuff_t Movies[];

//===========================================================================
//
//								     Prototypes
//
//===========================================================================

void MOVIE_ShowFrame (char *inpic);
bool MOVIE_Play(MovieStuff_t *MovieStuff);
void SetupMovie(MovieStuff_t *MovieStuff);
void ShutdownMovie(void);

#endif